const format = {
  numberFormat(value) {
    const formatedNumber = new Intl.NumberFormat("en-IN").format(value);
    return formatedNumber
  },
};

export default format;
