import axiosInstance from "./axiosInstance";

const apicalls = {
  getUserProfile(profileData) {
    axiosInstance()
      .get("/consumers/profile/")
      .then((res) => {
        if (res) {
          profileData(res.data);
        }
      });
  },

  getTotalAmount(totalAmountData) {
    axiosInstance()
      .get("/accounts/total-amount/")
      .then((res) => {
        totalAmountData(res.data);
      })
      .catch((err) => {
        if (err.response) {
          console.log(err.response.data.details);
        }
      });
  },
};

export default apicalls;
