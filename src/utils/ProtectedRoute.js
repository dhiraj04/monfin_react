import { Route, Redirect } from "react-router";
import isAuthenticated from "./isAuthenticated";

const ProtectedRoute = ({ component: Comp, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) => {
        return isAuthenticated() ? (
          <Comp {...props} />
        ) : (
          <Redirect to="/login" />
        );
      }}
    />
  );
};

export default ProtectedRoute;
