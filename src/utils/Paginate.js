import React, { Component } from "react";
import { Pagination } from "react-bootstrap";

export class Paginate extends Component {
  render() {
    const {
      postsPerPage,
      totalPosts,
      paginate,
      nextPage,
      prevPage,
      currentPage,
    } = this.props;

    
    const pageNumbers = [];

    for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
      pageNumbers.push(i);
    }
    return (
      <>
        <Pagination>
          <Pagination.Prev onClick={() => prevPage()} />
          {pageNumbers.map((num) => (
            <Pagination.Item
              key={num}
              onClick={() => paginate(num)}
              active={num === currentPage}
            >
              {num}
            </Pagination.Item>
          ))}
          <Pagination.Next onClick={() => nextPage()} />
        </Pagination>
      </>
    );
  }
}

export default Paginate;
