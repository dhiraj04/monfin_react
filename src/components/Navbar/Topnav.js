import { Component } from "react";
import { Nav, Navbar } from "react-bootstrap";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { logout } from "../../features/userSlice/userSlice";
import { history } from "../../history";
import axiosInstance from "../../utils/axiosInstance";

class Topnav extends Component {
  handleLogout = () => {
    axiosInstance()
      .post("/user/logout/")
      .then((res) => {
        this.props.logout();
        localStorage.removeItem("token");
        history.push("/login");
      });
  };

  render() {
    return (
      <Navbar bg="primary" variant="dark">
        <Navbar.Brand as={Link} to="/">
          Paisa!! Paisa!!
        </Navbar.Brand>
        <Nav className="ml-auto">
          <Nav.Link as={Link} to="/">
            Home
          </Nav.Link>
          <Nav.Link as={Link} to="/money/">
            Money
          </Nav.Link>
          <Nav.Link as={Link} to="/pay-receive/">
            PayReceive
          </Nav.Link>
          <Nav.Link as={Link} to="/profile/">
            Profile
          </Nav.Link>
          <Nav.Link onClick={this.handleLogout}>Logout</Nav.Link>
        </Nav>
      </Navbar>
    );
  }
}

const mapDispatchToProps = {
  logout,
};

export default connect(null, mapDispatchToProps)(Topnav);
