import React from "react";
import {
  Button,
  Card,
  Col,
  Container,
  Row,
  Spinner,
  Table,
} from "react-bootstrap";
import Topnav from "../Navbar/Topnav";
import axiosInstance from "../../utils/axiosInstance";
import NewAccount from "./Accounts/NewAccount";
import NewAccountGroup from "./Accounts/NewAccountGroup";
import EditAccountGroup from "./Accounts/EditAccountGroup";
import EditAccount from "./Accounts/EditAccount";
import { connect } from "react-redux";
import { totalAmountData } from "../../features/userSlice/TotalAmountSlice";
import apicalls from "../../utils/apiCalls";
import { profileData } from "../../features/userSlice/ProfileSlice";
import { Paginate } from "../../utils/Paginate";
import MoneyChart from "./Money/MoneyChart";

class Dashboard extends React.Component {
  state = {
    accountModalshow: false,
    editAccountModalShow: false,
    accounts_data: "",
    edit_account_id: "",
    account_is_loading: true,
    account_error_message: undefined,

    edit_account_group_id: "",
    account_group_data: "",
    accountGroupModalShow: false,
    editAccountGroupModalShow: false,
    account_group_is_loading: true,
    account_group_error_message: undefined,

    currentPage: 1,
    postPerPage: 10,
  };

  componentDidMount() {
    this.getAccountsList();
    this.getAccountGroupLists();
    apicalls.getTotalAmount(this.props.totalAmountData);
    apicalls.getUserProfile(this.props.profileData);
  }

  getAccountsList() {
    axiosInstance()
        .get("/accounts/")
        .then((res) => {
          this.setState({
            accounts_data: res.data,
            account_is_loading: false,
          });
        })
        .catch((err) => {
          if (err.response) {
            this.setState({
              account_is_loading: true,
              account_error_message: err.response.data,
            });
          }
        });
  }

  getAccountGroupLists() {
    axiosInstance(this.props.history)
        .get("/accounts/groups/")
        .then((res) => {
          this.setState({
            account_group_data: res.data,
            account_groups: res.data,
            account_group_is_loading: false,
          });
        })
        .catch((err) => {
          if (err.response) {
            this.setState({
              account_group_is_loading: true,
              account_group_error_message: err.response.data,
            });
          }
        });
  }

  handleModalClose = () => {
    this.setState({ accountModalshow: false });
  };

  handleEditAccountModalClose = () => {
    this.setState({ editAccountModalShow: false });
  };

  handleAccountGroupModalClose = () => {
    this.setState({ accountGroupModalShow: false });
  };

  handleEditAccountGroupModalClose = () => {
    this.setState({ editAccountGroupModalShow: false });
  };

  render() {
    const total_amount = this.props.totalAmount;
    const profile = this.props.profile;

    const accounts = this.state.accounts_data;
    const account_loading = this.state.account_is_loading;

    const account_groups = this.state.account_group_data;
    const account_group_is_loading = this.state.account_group_is_loading;

    const { currentPage, postPerPage } = this.state;

    const indexOfLastPost = currentPage * postPerPage;
    const indexOfFirstPost = indexOfLastPost - postPerPage;
    const currentPost = accounts.slice(indexOfFirstPost, indexOfLastPost);

    const paginate = (pageNum) => this.setState({ currentPage: pageNum });
    const nextPage = () => this.setState({ currentPage: currentPage + 1 });
    const prevPage = () => this.setState({ currentPage: currentPage - 1 });

    return (
        <>
          <Topnav />
          <Container fluid className="pr-5 pl-5 pb-2 pt-2">
            <Row className="mb-1">
              <Col md={6}>{profile && <p>{profile.full_name}</p>}</Col>
              <Col md={6}>
                <Card className="shadow rounded-0 border-0 bg-success text-white float-right">
                  <Card.Body>
                    {total_amount && (
                        <h5 className="text-center">
                          Total: रू {total_amount.total_amount}
                        </h5>
                    )}
                  </Card.Body>
                </Card>
              </Col>
            </Row>

            <Row>
              <Col md={6}>
                <Card className='shadow rounded-0'>
                  <Card.Header>
                    {account_loading && (
                        <Spinner animation="border" role="status" variant="primary" />
                    )}
                    <Button
                        className="btn btn-primary rounded-0 border-0 shadow mb-2"
                        onClick={() => this.setState({ accountModalshow: true })}
                    >
                      New Account
                    </Button>

                    {this.state.accountModalshow && (
                        <NewAccount
                            onHide={this.handleModalClose}
                            onAccountAdd={() => this.getAccountsList()}
                        />
                    )}
                  </Card.Header>
                  <Card.Body>
                    <Table striped bordered hover responsive="md">
                      <thead>
                      <tr>
                        <th>#</th>
                        <th>Account Name</th>
                        <th>Amount</th>
                        <th>Actions</th>
                      </tr>
                      </thead>
                      <tbody>
                      {currentPost &&
                      currentPost.map((account, index) => (
                          <tr key={account.id}>
                            <td>{index + 1}</td>
                            <td>{account.account_name}</td>
                            <td>{account.amount}</td>
                            <td>
                              <Button
                                  className="rounded-0 border-0"
                                  variant="info"
                                  size="sm"
                                  onClick={() =>
                                      this.setState({
                                        editAccountModalShow: true,
                                        edit_account_id: account.id,
                                      })
                                  }
                              >
                                Edit
                              </Button>
                            </td>
                          </tr>
                      ))}
                      </tbody>
                    </Table>
                    <Paginate
                        postsPerPage={postPerPage}
                        totalPosts={accounts.length}
                        paginate={paginate}
                        nextPage={nextPage}
                        prevPage={prevPage}
                        currentPage={currentPage}
                    />
                  </Card.Body>
                </Card>
              </Col>

              <Col md={6}>
                <Card>
                  <Card.Header>
                    {account_group_is_loading && (
                        <Spinner animation="border" role="status" variant="primary" />
                    )}

                    <Button
                        className="btn btn-primary rounded-0 border-0 shadow mb-2"
                        onClick={() => this.setState({ accountGroupModalShow: true })}
                    >
                      New Account Group
                    </Button>

                    {this.state.accountGroupModalShow && (
                        <NewAccountGroup
                            onHide={this.handleAccountGroupModalClose}
                            onAccountGroupAdd={() => this.getAccountGroupLists()}
                        />
                    )}
                  </Card.Header>
                  <Card.Body>
                    <Table striped bordered hover>
                      <thead>
                      <tr>
                        <th>#</th>
                        <th>Account Group</th>
                        <th>Actions</th>
                      </tr>
                      </thead>
                      <tbody>
                      {account_groups &&
                      account_groups.map((account_group, index) => (
                          <tr key={account_group.id}>
                            <td>{index + 1}</td>
                            <td>{account_group.group_name}</td>
                            <td>
                              <Button
                                  className="rounded-0 border-0"
                                  variant="info"
                                  size="sm"
                                  onClick={() =>
                                      this.setState({
                                        editAccountGroupModalShow: true,
                                        account_group: account_group.group_name,
                                        edit_account_group_id: account_group.id,
                                      })
                                  }
                              >
                                Edit
                              </Button>
                            </td>
                          </tr>
                      ))}
                      </tbody>
                    </Table>
                  </Card.Body>
                </Card>


              </Col>
            </Row>

            <Row className='m-4'>
              <Col>
                <Card className='shadow rounded-0'>
                  <Card.Body>
                    <MoneyChart/>
                  </Card.Body>
                </Card>
              </Col>
            </Row>

            {this.state.editAccountModalShow && (
                <EditAccount
                    id={this.state.edit_account_id}
                    onHide={this.handleEditAccountModalClose}
                    onAccountEdit={() => this.getAccountsList()}
                />
            )}

            {this.state.editAccountGroupModalShow && (
                <EditAccountGroup
                    id={this.state.edit_account_group_id}
                    onHide={this.handleEditAccountGroupModalClose}
                    onAccountGroupEdit={() => this.getAccountGroupLists()}
                />
            )}
          </Container>
        </>
    );
  }
}

const mapDispatchToProps = {
  totalAmountData,
  profileData,
};

const mapStateToProps = (state) => {
  return {
    totalAmount: state.totalAmount.data,
    profile: state.profile.data,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
