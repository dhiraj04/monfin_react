import React, { Component } from "react";
import { Button, Card, Col, Form } from "react-bootstrap";
import { connect } from "react-redux";
import { totalAmountData } from "../../../../features/userSlice/TotalAmountSlice";
import apicalls from "../../../../utils/apiCalls";
import axiosInstance from "../../../../utils/axiosInstance";

export class ExpenseAdd extends Component {
  state = {
    expense_amount: "",
    remarks: "",
    account: "",
    expense_date: "",

    form_error_message: undefined,
    is_form_loading: false,
  };

  
  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = (e) => {
    e.preventDefault();
    this.setState({
      is_form_loading: true,
    });
    let expense_data = {
      expense_amount: this.state.expense_amount,
      remarks: this.state.remarks,
      account: this.state.account,
      expense_date: this.state.expense_date,
    };
    axiosInstance()
      .post("/money/expense/add/", expense_data)
      .then((res) => {
        this.setState({
          is_form_loading: false,
        });
        this.props.onExpenseAdd();
        apicalls.getTotalAmount(this.props.totalAmountData);
        this.clearForm();
      })
      .catch((err) => {
        if (err.response) {
          this.setState({
            is_form_loading: false,
            form_error_message: err.response.data.Amount,
          });
        }
      });
  };

  clearForm = () => {
    this.setState({
      expense_amount: "",
      remarks: "",
    });
  };

  render() {
    const form_accounts = this.props.form_accounts;
    const ExpenseFormValid =
      !this.state.expense_amount?.length ||
      !this.state.remarks?.length ||
      !this.state.account?.length;

    return (
      <>
        <Col md={6}>
          <Card className="shadow rounded-0">
            <Card.Body>
              <Form>
                <Form.Row>
                  <Form.Group as={Col} controlId="formAddExpenseAmount">
                    <Form.Label>Expense Amount</Form.Label>
                    <Form.Control
                      value={this.state.expense_amount}
                      onChange={this.onChange}
                      type="number"
                      step=".01"
                      name="expense_amount"
                      placeholder="1500.50"
                      required
                      isInvalid={!!this.state.form_error_message}
                    />
                    <Form.Control.Feedback type="invalid">
                      {this.state.form_error_message}
                    </Form.Control.Feedback>
                  </Form.Group>
                  <Form.Group as={Col} controlId="formAddExpenseAccount">
                    <Form.Label>Account</Form.Label>
                    <Form.Control
                      onChange={this.onChange}
                      as="select"
                      name="account"
                      defaultValue="Choose Account"
                      required
                    >
                      <option>Choose Account</option>
                      {form_accounts &&
                        form_accounts.map((account, index) => (
                          <option
                            key={account.id}
                            value={account.id}
                            name="account"
                          >
                            {account.account_name}
                          </option>
                        ))}
                    </Form.Control>
                  </Form.Group>
                  <Form.Group as={Col} controlId="formAddExpenseRemakrs">
                    <Form.Label>Remarks</Form.Label>
                    <Form.Control
                      value={this.state.remarks}
                      onChange={this.onChange}
                      type="text"
                      name="remarks"
                      placeholder="mo:mo Party"
                      required
                    />
                  </Form.Group>
                </Form.Row>

                <Form.Row>
                  <Form.Group as={Col} md="4" controlId="formAddExpenseDate">
                    <Form.Label>Expense Date</Form.Label>
                    <Form.Control
                      value={this.state.expense_date}
                      onChange={this.onChange}
                      type="date"
                      name="expense_date"
                      placeholder="Select Date"
                      required
                    />
                  </Form.Group>
                </Form.Row>

                <Button
                  className="float-right"
                  variant="primary"
                  type="submit"
                  disabled={ExpenseFormValid || this.state.is_form_loading}
                  onClick={this.onSubmit}
                >
                  {this.state.is_form_loading ? "Adding" : "Add"}
                </Button>
              </Form>
            </Card.Body>
          </Card>
        </Col>
      </>
    );
  }
}

const mapDispatchToProps = {
  totalAmountData,
};

export default connect(null, mapDispatchToProps)(ExpenseAdd);
