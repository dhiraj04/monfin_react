import React, { Component } from "react";
import { Button, Col, Form, Modal } from "react-bootstrap";
import { connect } from "react-redux";
import { totalAmountData } from "../../../../features/userSlice/TotalAmountSlice";
import apicalls from "../../../../utils/apiCalls";
import axiosInstance from "../../../../utils/axiosInstance";

export class ExpenseEdit extends Component {
  state = {
    expense_amount: "",
    remarks: "",
    account: "",
    expense_date: '',

    form_error_message: undefined,
    is_form_loading: false,
  };

  componentDidMount() {
    this.getExpenseDetaiilsForEdit();
  }

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  getExpenseDetaiilsForEdit = () => {
    axiosInstance()
      .get(`/money/expense/edit/${this.props.id}/`)
      .then((res) => {
        this.setState({
          expense_amount: res.data.expense_amount,
          remarks: res.data.remarks,
          account: res.data.account.id,
          expense_date: res.data.expense_date,
        });
      })
      .catch((err) => {
        if (err.response) {
          console.log(err.response.data.detail);
        }
      });
  };

  onSubmit = () => {
    this.setState({ is_form_loading: true });

    let expense_data = {
      expense_amount: this.state.expense_amount,
      remarks: this.state.remarks,
      account: this.state.account,
      expense_date: this.state.expense_date
    };

    axiosInstance()
      .put(`/money/expense/edit/${this.props.id}/`, expense_data)
      .then((res) => {
        this.setState({
          is_form_loading: false,
        });
        this.props.onExpenseEdit();
        this.props.onHide();
        apicalls.getTotalAmount(this.props.totalAmountData)
      })
      .catch((err) => {
        if (err.response) {
          this.setState({
            is_form_loading: false,
          });
          console.log(err.response.data);
        }
      });
  };

  render() {
    const form_accounts = this.props.forms_accounts;
    return (
      <Form>
        <Modal show={true} onHide={this.props.onHide} size="lg">
          <Modal.Header closeButton>
            <Modal.Title>Update Expense Amount</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Row>
              <Form.Group as={Col} controlId="formUpdateExpenseAmount">
                <Form.Label>Account Name</Form.Label>
                <Form.Control
                  value={this.state.expense_amount}
                  onChange={this.onChange}
                  type="number"
                  step="0.01"
                  name="expense_amount"
                  placeholder="100.75"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} controlId="formUpdateExpenseAccount">
                <Form.Label>Account</Form.Label>
                <Form.Control
                  value={this.state.account}
                  onChange={this.onChange}
                  as="select"
                  name="account"
                  required
                >
                  <option>Choose Account</option>
                  {form_accounts &&
                    form_accounts.map((account, index) => (
                      <option
                        key={account.id}
                        value={account.id}
                        name="account"
                      >
                        {account.account_name}
                      </option>
                    ))}
                </Form.Control>
              </Form.Group>
              <Form.Group as={Col} controlId="formUpdateExpenseRemakrs">
                <Form.Label>Remarks</Form.Label>
                <Form.Control
                  value={this.state.remarks}
                  onChange={this.onChange}
                  type="text"
                  name="remarks"
                  placeholder="Baishak Salary"
                  required
                />
              </Form.Group>
            </Form.Row>

            <Form.Row>
              <Form.Group as={Col} md="4" controlId="formUpdateExpenseDate">
                <Form.Label>Expense Date</Form.Label>
                <Form.Control
                    value={this.state.expense_date}
                    onChange={this.onChange}
                    type="date"
                    name="expense_date"
                    placeholder="Select Date"
                    required
                />
              </Form.Group>
            </Form.Row>

          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.props.onHide}>
              Close
            </Button>
            <Button
              variant="primary"
              type="submit"
              disabled={this.state.is_form_loading}
              onClick={this.onSubmit}
            >
              {this.state.is_form_loading ? "Updating Expense" : "Update"}
            </Button>
          </Modal.Footer>
        </Modal>
      </Form>
    );
  }
}

const mapDispatchToProps = {
  totalAmountData
};

export default connect(null, mapDispatchToProps)(ExpenseEdit);
