import React, { Component } from "react";
import {Spinner, Table, Button } from "react-bootstrap";
import ExpenseEdit from "./ExpenseEdit";

export class ExpenseList extends Component {
  state = {
    editExpenseID: "",
    expenseEditModalShow: false,
  };

  expenseEditModalClose = () => {
    this.setState({
      expenseEditModalShow: false,
    });
  };

  render() {
    const forms_accounts = this.props.form_accounts;
    const expenseList = this.props.expense_data;
    const is_loading = this.props.expense_is_loading;
    return (
      <>
        <Table>
          <thead>
            <tr className="text-center">
              <th>#</th>
              <th>Account</th>
              <th>Expense Amount</th>
              <th>Date</th>
              <th>Remarks</th>
              <th>Actions</th>
            </tr>
          </thead>
          {is_loading && (
            <Spinner animation="border" role="status" variant="primary" />
          )}
          <tbody>
            {expenseList &&
              expenseList.map((expense, index) => (
                <tr className="text-center" key={expense.id}>
                  <td>{index + 1}</td>
                  <td>{expense.account}</td>
                  <td>{expense.expense_amount}</td>
                  <td>{expense.expense_date}</td>
                  <td>{expense.remarks}</td>
                  <td>
                    <Button
                      variant="info"
                      size="sm"
                      className="rounded-0 border-0 shadow"
                      onClick={() =>
                        this.setState({
                          editExpenseID: expense.id,
                          expenseEditModalShow: true,
                        })
                      }
                    >
                      Update
                    </Button>
                  </td>
                </tr>
              ))}
          </tbody>
        </Table>

        {this.state.expenseEditModalShow && (
          <ExpenseEdit
            forms_accounts={forms_accounts}
            id={this.state.editExpenseID}
            onHide={this.expenseEditModalClose}
            onExpenseEdit={this.props.onExpenseEdit}
          />
        )}
      </>
    );
  }
}

export default ExpenseList;
