import React, { Component } from "react";
import { Button, Col, Form, Modal } from "react-bootstrap";
import { connect } from "react-redux";
import { totalAmountData } from "../../../../features/userSlice/TotalAmountSlice";
import apicalls from "../../../../utils/apiCalls";
import axiosInstance from "../../../../utils/axiosInstance";

export class IncomeEdit extends Component {
  state = {
    income_amount: "",
    remarks: "",
    account: "",
    income_date: "",

    form_error_message: undefined,
    is_form_loading: false,
  };

  componentDidMount() {
    this.getIncomeDetaiilsForEdit();
  }

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  getIncomeDetaiilsForEdit = () => {
    axiosInstance()
      .get(`/money/income/edit/${this.props.id}/`)
      .then((res) => {
        this.setState({
          income_amount: res.data.income_amount,
          remarks: res.data.remarks,
          account: res.data.account.id,
          income_date: res.data.income_date,
        });
      })
      .catch((err) => {
        if (err.response) {
          console.log(err.response.data.detail);
        }
      });
  };

  onSubmit = () => {
    this.setState({ is_form_loading: true });

    let income_data = {
      income_amount: this.state.income_amount,
      remarks: this.state.remarks,
      account: this.state.account,
      income_date: this.state.income_date
    };

    axiosInstance()
      .put(`/money/income/edit/${this.props.id}/`, income_data)
      .then((res) => {
        this.setState({
          is_form_loading: false,
        });
        this.props.onIncomeEdit();
        apicalls.getTotalAmount(this.props.totalAmountData)
        this.props.onHide();
      })
      .catch((err) => {
        if (err.response) {
          console.log(err.response.data.detail);
        }
      });
  };

  render() {
    const form_accounts = this.props.forms_accounts;
    return (
      <Form>
        <Modal show={true} onHide={this.props.onHide} size="lg">
          <Modal.Header closeButton>
            <Modal.Title>Update Income Amount</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Row>
              <Form.Group as={Col} controlId="formUpdateIncomeAmount">
                <Form.Label>Account Name</Form.Label>
                <Form.Control
                  value={this.state.income_amount}
                  onChange={this.onChange}
                  type="number"
                  step="0.01"
                  name="income_amount"
                  placeholder="100.75"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} controlId="formUpdateIncomeAccount">
                <Form.Label>Account</Form.Label>
                <Form.Control
                  value={this.state.account}
                  onChange={this.onChange}
                  as="select"
                  name="account"
                  required
                >
                  <option>Choose Account</option>
                  {form_accounts &&
                    form_accounts.map((account, index) => (
                      <option
                        key={account.id}
                        value={account.id}
                        name="account"
                      >
                        {account.account_name}
                      </option>
                    ))}
                </Form.Control>
              </Form.Group>
              <Form.Group as={Col} controlId="formUpdateIncomeRemakrs">
                <Form.Label>Remarks</Form.Label>
                <Form.Control
                  value={this.state.remarks}
                  onChange={this.onChange}
                  type="text"
                  name="remarks"
                  placeholder="Baishak Salary"
                  required
                />
              </Form.Group>
            </Form.Row>

            <Form.Row>
              <Form.Group as={Col} md="4" controlId="formUpdateIncomeDate">
                <Form.Label>Income Date</Form.Label>
                <Form.Control
                    value={this.state.income_date}
                    onChange={this.onChange}
                    type="date"
                    name="income_date"
                    placeholder="Select Date"
                    required
                />
              </Form.Group>
            </Form.Row>

          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.props.onHide}>
              Close
            </Button>
            <Button
              variant="primary"
              type="submit"
              disabled={this.state.is_form_loading}
              onClick={this.onSubmit}
            >
              {this.state.is_form_loading ? "Updating Income" : "Update"}
            </Button>
          </Modal.Footer>
        </Modal>
      </Form>
    );
  }
}

const mapDispatchToProps = {
  totalAmountData
};

export default connect(null, mapDispatchToProps)(IncomeEdit);

