import React, { Component } from "react";
import { Button, Spinner, Table } from "react-bootstrap";
import IncomeEdit from "./IncomeEdit";

export class IncomeList extends Component {
  state = {
    editIncomeID: "",
    incomeEditModalShow: false,
  };

  incomeEditModalClose = () => {
    this.setState({
      incomeEditModalShow: false,
    });
  };

  render() {
    const forms_accounts = this.props.form_accounts;
    const incomeList = this.props.income_data;
    const is_loading = this.props.income_is_loading;
    return (
      <>
        <Table>
          <thead>
            <tr className="text-center">
              <th>#</th>
              <th>Account</th>
              <th>Income Amount</th>
              <th>Date</th>
              <th>Remarks</th>
              <th>Actions</th>
            </tr>
          </thead>
          {is_loading && (
            <Spinner animation="border" role="status" variant="primary" />
          )}
          <tbody>
            {incomeList &&
              incomeList.map((income, index) => (
                <tr className="text-center" key={income.id}>
                  <td>{index + 1}</td>
                  <td>{income.account}</td>
                  <td>{income.income_amount}</td>
                  <td>{income.income_date}</td>
                  <td>{income.remarks}</td>
                  <td>
                    <Button
                      variant="info"
                      size="sm"
                      className="rounded-0 border-0 shadow"
                      onClick={() =>
                        this.setState({
                          editIncomeID: income.id,
                          incomeEditModalShow: true,
                        })
                      }
                    >
                      Update
                    </Button>
                  </td>
                </tr>
              ))}
          </tbody>
        </Table>
        {this.state.incomeEditModalShow && (
          <IncomeEdit
            forms_accounts={forms_accounts}
            id={this.state.editIncomeID}
            onHide={this.incomeEditModalClose}
            onIncomeEdit={this.props.onIncomeEdit}
          />
        )}
      </>
    );
  }
}

export default IncomeList;
