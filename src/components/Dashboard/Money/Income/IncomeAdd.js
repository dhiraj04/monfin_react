import React, { Component } from "react";
import { Button, Card, Col, Form } from "react-bootstrap";
import { connect } from "react-redux";
import { totalAmountData } from "../../../../features/userSlice/TotalAmountSlice";
import apicalls from "../../../../utils/apiCalls";
import axiosInstance from "../../../../utils/axiosInstance";

export class IncomeAdd extends Component {
  state = {
    income_amount: "",
    remarks: "",
    account: "",
    income_date: "",

    form_error_message: undefined,
    is_form_loading: false,
  };

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = (e) => {
    e.preventDefault();
    this.setState({
      is_form_loading: true,
    });
    let income_data = {
      income_amount: this.state.income_amount,
      remarks: this.state.remarks,
      account: this.state.account,
      income_date: this.state.income_date,
    };
    axiosInstance()
      .post("/money/income/add/", income_data)
      .then((res) => {
        this.setState({
          is_form_loading: false,
        });
        this.props.onIncomeAdd();
        apicalls.getTotalAmount(this.props.totalAmountData);
        this.clearForm();
      })
      .catch((err) => {
        if (err.response) {
          this.setState({
            form_error_message: err.response.data,
          });
        }
      });
  };

  clearForm = () => {
    this.setState({
      income_amount: "",
      remarks: "",
      income_date: '',
    });
  };

  render() {
//    const date = new Date()
//    console.log(date)

//    const new_date = moment(date).format("YYYY-MM-DD")
//    console.log(new_date)

    const form_accounts = this.props.form_accounts;
    const IncomeFormValid =
      !this.state.income_amount?.length ||
      !this.state.remarks?.length ||
      !this.state.account?.length;


    return (
      <>
        <Col md={6}>
          <Card className="shadow rounded-0">
            <Card.Body>
              <Form>
                <Form.Row>
                  <Form.Group as={Col} controlId="formAddIncomeAmount">
                    <Form.Label>Income Amount</Form.Label>
                    <Form.Control
                      value={this.state.income_amount}
                      onChange={this.onChange}
                      type="number"
                      step="0.01"
                      name="income_amount"
                      placeholder="100.75"
                      required
                    />
                  </Form.Group>
                  <Form.Group as={Col} controlId="formAddIncomeAccount">
                    <Form.Label>Account</Form.Label>
                    <Form.Control
                      onChange={this.onChange}
                      as="select"
                      name="account"
                      defaultValue="Choose Account"
                      required
                    >
                      <option>Choose Account</option>
                      {form_accounts &&
                        form_accounts.map((account, index) => (
                          <option
                            key={account.id}
                            value={account.id}
                            name="account"
                          >
                            {account.account_name}
                          </option>
                        ))}
                    </Form.Control>
                  </Form.Group>
                  <Form.Group as={Col} controlId="formAddIncomeRemakrs">
                    <Form.Label>Remarks</Form.Label>
                    <Form.Control
                      value={this.state.remarks}
                      onChange={this.onChange}
                      type="text"
                      name="remarks"
                      placeholder="Baishak Salary"
                      required
                    />
                  </Form.Group>
                </Form.Row>

                <Form.Row>
                  <Form.Group as={Col} md="4" controlId="formAddIncomeDate">
                    <Form.Label>Income Date</Form.Label>
                    <Form.Control
                      value={this.state.income_date}
                      onChange={this.onChange}
                      type="date"
                      name="income_date"
                      placeholder="Select Date"
                      required
                    />
                  </Form.Group>
                </Form.Row>

                <Button
                  className="float-right"
                  variant="primary"
                  type="submit"
                  disabled={IncomeFormValid || this.state.is_form_loading}
                  onClick={this.onSubmit}
                >
                  {this.state.is_form_loading ? "Adding" : "Add"}
                </Button>
              </Form>
            </Card.Body>
          </Card>
        </Col>
      </>
    );
  }
}

const mapDispatchToProps = {
  totalAmountData,
};

export default connect(null, mapDispatchToProps)(IncomeAdd);
