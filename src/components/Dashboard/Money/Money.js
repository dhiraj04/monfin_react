import React, { Component } from "react";
import { Container, Row, Col, Card } from "react-bootstrap";
import { connect } from "react-redux";
import axiosInstance from "../../../utils/axiosInstance";
import Paginate from "../../../utils/Paginate";
import Topnav from "../../Navbar/Topnav";
import ExpenseAdd from "./Expense/ExpenseAdd";
import ExpenseList from "./Expense/ExpenseList";
import IncomeAdd from "./Income/IncomeAdd";
import IncomeList from "./Income/IncomeList";

export class Money extends Component {
  state = {
    income_data: "",
    income_is_loading: false,
    income_error_message: undefined,

    expense_data: "",
    expense_is_loading: false,
    expense_error_message: undefined,

    form_accounts: "",

    currentPage: 1,
    postPerPage: 10,
  };

  componentDidMount() {
    this.getIncomeList();
    this.getExpenseList();
    this.getAccountsList();
  }

  getIncomeList() {
    this.setState({
      income_is_loading: true,
    });
    axiosInstance()
      .get("/money/income/")
      .then((res) => {
        this.setState({
          income_data: res.data,
          income_is_loading: false,
        });
      })
      .catch((err) => {
        if (err.response) {
          this.setState({
            income_is_loading: false,
            income_error_message: err.response.data,
          });
        }
      });
  }

  getExpenseList() {
    this.setState({
      expense_is_loading: true,
    });
    axiosInstance()
      .get("/money/expense/")
      .then((res) => {
        this.setState({
          expense_data: res.data,
          expense_is_loading: false,
        });
      })
      .catch((err) => {
        if (err.response) {
          this.setState({
            expense_is_loading: false,
            expense_error_message: err.response.data,
          });
        }
      });
  }

  getAccountsList() {
    axiosInstance()
      .get("/accounts/")
      .then((res) => {
        this.setState({
          form_accounts: res.data,
        });
      })
      .catch((err) => {
        if (err.response) {
          console.log(err.response.data.details);
        }
      });
  }

  render() {
    const total_amount = this.props.totalAmount;
    const { income_data, expense_data, currentPage, postPerPage } = this.state;

    const indexOfLastPost = currentPage * postPerPage;
    const indexOfFirstPost = indexOfLastPost - postPerPage;
    const IncomecurrentPost = income_data.slice(indexOfFirstPost, indexOfLastPost);
    const ExpensecurrentPost = expense_data.slice(indexOfFirstPost, indexOfLastPost);

    const paginate = (pageNum) => this.setState({ currentPage: pageNum });
    const nextPage = () => this.setState({ currentPage: currentPage + 1 });
    const prevPage = () => this.setState({ currentPage: currentPage - 1 });

    return (
      <>
        <Topnav />
        <Container fluid className="pr-5 pl-5 mt-2">
          <Row className="mb-1">
            <Col md={12}>
              <Card className="shadow rounded-0 border-0 bg-success text-white float-right">
                <Card.Body>
                  {total_amount && (
                    <h5 className="text-center">
                      Total: रू {total_amount.total_amount}
                    </h5>
                  )}
                </Card.Body>
              </Card>
            </Col>
          </Row>
          <Row>
            <IncomeAdd
              form_accounts={this.state.form_accounts}
              onIncomeAdd={() => this.getIncomeList()}
            />
            <ExpenseAdd
              form_accounts={this.state.form_accounts}
              onExpenseAdd={() => this.getExpenseList()}
            />
          </Row>
          <Row>
            <Col>
              <IncomeList
                form_accounts={this.state.form_accounts}
                onIncomeEdit={() => this.getIncomeList()}
                income_data={IncomecurrentPost}
//                income_data={this.state.income_data}
                income_is_loading={this.state.income_is_loading}
              />
              <Paginate
                postsPerPage={postPerPage}
                totalPosts={income_data.length}
                paginate={paginate}
                nextPage={nextPage}
                prevPage={prevPage}
                currentPage={currentPage}
              />
            </Col>
            <Col>
              <ExpenseList
                form_accounts={this.state.form_accounts}
                onExpenseEdit={() => this.getExpenseList()}
                expense_data={ExpensecurrentPost}
//                expense_data={this.state.expense_data}
                expense_is_loading={this.state.expense_is_loading}
              />
              <Paginate
                postsPerPage={postPerPage}
                totalPosts={expense_data.length}
                paginate={paginate}
                nextPage={nextPage}
                prevPage={prevPage}
                currentPage={currentPage}
              />
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    totalAmount: state.totalAmount.data,
  };
};

export default connect(mapStateToProps, null)(Money);
