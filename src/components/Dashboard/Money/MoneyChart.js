import React, {Component} from 'react';
import axiosInstance from "../../../utils/axiosInstance";
import {Bar } from 'react-chartjs-2';

class MoneyChart extends Component {

    state = {
        labels: '',
        income: '',
        expense: '',

        error_message: ''
    };

    componentDidMount() {
        this.getdata();
    }

    getdata() {
        axiosInstance()
            .get('/money/report/')
            .then((res) => {
                this.setState({
                    income: res.data.income_data,
                    expense: res.data.expense_data,
                    labels: res.data.labels
                })
            })
            .catch((err) =>{
                if(err.response){
                    this.setState({
                        error_message: err.response.data
                    });
                    console.log(err.response.data)
                }
            })
    }

    render() {

        const expense = this.state.expense;
        const income = this.state.income;
        const labels = this.state.labels;


        const data = {
            //labels: Array.from(labels).map(row => row.labels),
            labels: labels,
            datasets: [
                {
                    label: 'Income',
                    //data: Array.from(income).map(row => row.income_data),
                    data: income,
                    backgroundColor: 'lightgreen',
                    barThickness: 50,
                },
                {
                    label: 'Expense',
                    //data: Array.from(expense).map(row => row.expense_data),
                    data: expense,
                    backgroundColor: '#ffcccb',
                    barThickness: 50,
                }
            ],
        };

        const options = {
            responsive: true,
            scales: {
                yAxes: [
                    {
                        ticks: {
                            beginAtZero: true,
                        },
                        scales: {
                            display: true,
                            text: 'Money in Rupees',
                            fontSize: 18,
                        }

                    },
                ],
            },
            borderWidth: 0,
        };


        return (
            <>
                <div className='header'>
                    <h1 className='title text-center'>Income Expense For {new Date().getFullYear()}</h1>
                </div>
                <Bar data={data} height='100' options={options} />
            </>
        );
    }
}

export default MoneyChart;