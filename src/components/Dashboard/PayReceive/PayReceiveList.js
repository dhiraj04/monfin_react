import React, { Component } from "react";
import { Button, Spinner, Table } from "react-bootstrap";
import PayReceiveGroupEdit from "./PayReceiveGroupEdit";

export class PayReceiveList extends Component {
  state = {
    editPayReceiveGroupID: "",
    PayReceiveGroupModalShow: false,
  };

  PayReceiveGroupModalClose = () => {
    this.setState({
      PayReceiveGroupModalShow: false,
    });
  };

  render() {
    const total_pay_amount = this.props.total_pay_amount;
    const total_receive_amount = this.props.total_receive_amount;
    const payReceiveData = this.props.payReceiveGroup_data;
    const is_loading = this.props.is_loading;
    return (
      <>
        <Table>
          <thead>
            <tr className="text-center">
              <th>#</th>
              <th>Group Name</th>
              <th className="bg-danger text-white">Total Pay</th>
              <th className="bg-success text-white">Total Receive</th>
              <th>Paid Amount</th>
              <th>Received Amount</th>
              <th>Remaining Payable</th>
              <th>Remaining Receivable</th>
              <th>Actions</th>
            </tr>
          </thead>
          {is_loading && (
            <Spinner animation="border" role="status" variant="primary" />
          )}
          <tbody>
            {payReceiveData &&
              payReceiveData.map((data, index) => (
                <tr className="text-center" key={data.id}>
                  <td>{index + 1}</td>
                  <td>{data.name}</td>

                  {data.total_amount.pay_amount ? (
                    <td>{data.total_amount.pay_amount}</td>
                  ) : (
                    <td>-</td>
                  )}

                  {data.total_amount.receive_amount ? (
                    <td>{data.total_amount.receive_amount}</td>
                  ) : (
                    <td>-</td>
                  )}

                  {data.total_amount.paid_amount ? (
                      <td>{data.total_amount.paid_amount}</td>
                  ) : (
                      <td>-</td>
                  )}

                  {data.total_amount.received_amount ? (
                      <td>{data.total_amount.received_amount}</td>
                  ) : (
                      <td>-</td>
                  )}

                  {data.total_amount.remain_payable ? (
                      <td>{data.total_amount.remain_payable}</td>
                  ) : (
                      <td>-</td>
                  )}

                  {data.total_amount.remain_receivable ? (
                      <td>{data.total_amount.remain_receivable}</td>
                  ) : (
                      <td>-</td>
                  )}


                  <td>
                    <Button
                      variant="info"
                      size="sm"
                      className="rounded-0"
                      onClick={() =>
                        this.setState({
                          editPayReceiveGroupID: data.id,
                          PayReceiveGroupModalShow: true,
                        })
                      }
                    >
                      Edit
                    </Button>
                  </td>
                </tr>
              ))}
              <tr className='text-center border'>
                <td colSpan={6} className='text-right'><b>Total: </b></td>
                <td className='bg-danger text-white text-right'>{total_pay_amount}</td>
                <td className='bg-success text-white'>{total_receive_amount}</td>
                <td></td>
              </tr>
          </tbody>
        </Table>
        {this.state.PayReceiveGroupModalShow && (
          <PayReceiveGroupEdit
            id={this.state.editPayReceiveGroupID}
            onHide={this.PayReceiveGroupModalClose}
            onPayReceiveGroupEdit={this.props.onPayReceiveEdit}
          />
        )}
      </>
    );
  }
}

export default PayReceiveList;
