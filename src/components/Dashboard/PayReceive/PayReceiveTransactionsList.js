import React, {Component} from 'react';
import axiosInstance from "../../../utils/axiosInstance";
import { Row, Col, Table, Spinner, Button } from "react-bootstrap";
import PayReceiveTransactionsAdd from "./PayReceiveTransactionsAdd";

class PayReceiveTransactionsList extends Component {
    state = {
        is_loading: false,
        transactions_data: '',
        error_message: '',

        payReceiveTransactionModalShow: false
    };

    componentDidMount() {
        this.getPayReceiveTransactionsList();
    }


    getPayReceiveTransactionsList(){
        this.setState({
            is_loading: true
        });
        axiosInstance()
            .get('/pay-receive/transactions/')
            .then((res)=>{
                this.setState({
                    transactions_data: res.data,
                    is_loading: false
                })
            })
            .catch((err) =>{
                if(err.response){
                    this.setState({
                        error_message: err.response.data,
                        is_loading: false
                    })
                }
            })
    }

    payReceiveTransactionModalClose = () => {
        this.setState({
            payReceiveTransactionModalShow: false,
        });
    };

    render() {
        const transactions_data = this.state.transactions_data;
        const is_loading = this.state.is_loading;

        return (
            <>
                <Row className='mt-3'>
                    <Col md={6}>
                        <h2>Pay Receive Transactions</h2>
                    </Col>
                    <Col md={6}>
                        <Button
                            className="float-right rounded-0 shadow"
                            variant="warning"
                            onClick={() =>
                                this.setState({
                                    payReceiveTransactionModalShow: true,
                                })
                            }
                        >
                            PayReceive Transactions
                        </Button>

                        {this.state.payReceiveTransactionModalShow && (
                            <PayReceiveTransactionsAdd
                                onHide={this.payReceiveTransactionModalClose}
                                onPayReceivetransactionsAdd={() => this.getPayReceiveTransactionsList()}
                                onPayreceiveTransactionAdd_getGroup={this.props.onPayreceiveTransactionAdd_getGroup}
                            />
                        )}
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Table>
                            <thead>
                            <tr className="text-center">
                                <th>#</th>
                                <th>Person</th>
                                <th>Remarks</th>
                                <th>Paid</th>
                                <th>Received</th>
                                <th>Transaction Date</th>
                                {/*<th>Actions</th>*/}
                            </tr>
                            </thead>
                            {is_loading && (
                                <Spinner animation="border" role="status" variant="primary" />
                            )}
                            <tbody>
                            {transactions_data && transactions_data.map((data, index) => (
                                <tr key={data.id} className="text-center">
                                    <td>{index + 1}</td>
                                    <td>{data.pay_receive_group.name}</td>
                                    <td>{data.remarks}</td>

                                    {data.transaction_type === 'Pay' ? (
                                        <td>{data.amount}</td>
                                    ): (<td>-</td>)}

                                    {data.transaction_type === 'Receive' ? (
                                        <td>{data.amount}</td>
                                    ): (<td>-</td>)}

                                    <td>{data.transaction_date}</td>
                                </tr>
                            ))}
                            </tbody>

                        </Table>
                    </Col>
                </Row>
            </>
        );
    }
}

export default PayReceiveTransactionsList;