import React, { Component } from "react";
import { Button, Col, Form, Modal } from "react-bootstrap";
import axiosInstance from "../../../utils/axiosInstance";

export class PayReceiveAmountAdd extends Component {
  state = {
    is_form_loading: false,
    group: "",
    amount: "",
    remarks: "",
    pay_receive: "",
    error_message: undefined,
  };

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = (e) => {
    this.setState({ is_form_loading: true });
    let { group, amount, remarks, pay_receive } = this.state;
    let pay_receive_data = {
      group: group,
      amount: amount,
      remarks: remarks,
      pay_receive: pay_receive,
    };

    axiosInstance()
      .post("/pay-receive/amount/add/", pay_receive_data)
      .then((res) => {
        this.setState({
          is_form_loading: false,
        });
        this.props.getAccountGroup();
        this.props.onPayReceiveAmountAdd();
        this.props.onHide();
        console.log( this.props.onPayReceiveAmountAdd())
      })
      .catch((err) => {
        if (err.response) {
          this.setState({
            is_form_loading: false,
            error_message: err.response.data,
          });
        }
      });
  };

  render() {
    const payReceiveGroup_data = this.props.payReceiveGroup_data;
    const payReceiveAmountFormValid =
      !this.state.group?.length ||
      !this.state.amount?.length ||
      !this.state.remarks?.length ||
      !this.state.pay_receive?.length;

    return (
      <Form>
        <Modal show={true} onHide={this.props.onHide} size="lg">
          <Modal.Header closeButton>
            <Modal.Title>Add Pay Receive</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Row>
              <Form.Group as={Col} controlId="formAddPayReceiveAmountGroup">
                <Form.Label>Select Person</Form.Label>
                <Form.Control
                  as="select"
                  required
                  name="group"
                  onChange={this.onChange}
                  defaultValue="Chose Person"
                >
                  <option>Choose Person</option>
                  {payReceiveGroup_data &&
                    payReceiveGroup_data.map((data, index) => (
                      <option key={data.id} value={data.id} name="group">
                        {data.name}
                      </option>
                    ))}
                </Form.Control>
              </Form.Group>

              <Form.Group as={Col} md="3" controlId="formAddPayReceiveAmountamount">
                <Form.Label>Amount</Form.Label>
                <Form.Control
                  value={this.state.amount}
                  onChange={this.onChange}
                  name="amount"
                  type="number"
                  setp="0.01"
                  placeholder="Amount 100"
                  required
                />
              </Form.Group>

              <Form.Group as={Col} controlId="formAddPayReceiveAmountPayReceive">
                <Form.Label>PayReceive</Form.Label>
                <Form.Control
                  as="select"
                  required
                  name="pay_receive"
                  onChange={this.onChange}
                  defaultValue="Select Pay or Receive"
                >
                  <option>Select Pay or Receive</option>
                  <option value="Pay" name="pay_reveive">
                    Pay
                  </option>
                  <option value="Receive" name="pay_reveive">
                    Receive
                  </option>
                </Form.Control>
              </Form.Group>
            </Form.Row>

            <Form.Row>
              <Form.Group as={Col} controlId="formAddPayReceiveAmountremarks">
                <Form.Label>Remarks</Form.Label>
                <Form.Control
                  value={this.state.remarks}
                  onChange={this.onChange}
                  name="remarks"
                  type="text"
                  placeholder="Whatever"
                  required
                />
              </Form.Group>
            </Form.Row>
          </Modal.Body>

          <Modal.Footer>
            <Button variant="secondary" onClick={this.props.onHide}>
              Close
            </Button>
            <Button
              variant="primary"
              type="submit"
              disabled={payReceiveAmountFormValid || this.state.is_form_loading}
              onClick={this.onSubmit}
            >
              Add {this.state.is_form_loading ? "Adding" : "Add"}
            </Button>
          </Modal.Footer>
        </Modal>
      </Form>
    );
  }
}

export default PayReceiveAmountAdd;
