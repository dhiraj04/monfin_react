import React, {Component} from 'react';
import { Button, Col, Form, Modal } from "react-bootstrap";
import axiosInstance from "../../../utils/axiosInstance";

class PayReceiveTransactionsAdd extends Component {
    state = {
        pay_receive_group: "",
        amount: '',
        remarks: "",
        transaction_type: "",
        transaction_date: "",

        form_error_message: '',
        is_form_loading: false,

        payReceiveGroup_data: ''
    };

    componentDidMount() {
        this.getPayReceiveGroupList();
    }

    onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    };


    getPayReceiveGroupList() {
        axiosInstance()
            .get("/pay-receive/")
            .then((res) => {
                this.setState({ payReceiveGroup_data: res.data });
            });
    }

    onSubmit = (e) => {
        this.setState({ is_form_loading: true });
        let { pay_receive_group, amount, remarks, transaction_type, transaction_date } = this.state;
        let pay_receive_transactions_data = {
            pay_receive_group: pay_receive_group,
            amount: amount,
            remarks: remarks,
            transaction_type: transaction_type,
            transaction_date: transaction_date,
        };

        axiosInstance()
            .post('/pay-receive/transactions/add/', pay_receive_transactions_data)
            .then((res) => {
                this.setState({
                    is_form_loading: false,
                });
                this.props.onPayReceivetransactionsAdd();
                this.props.onPayreceiveTransactionAdd_getGroup();
                this.props.onHide();
            })
            .catch((err) => {
                if (err.response) {
                    this.setState({
                        is_form_loading: false,
                        form_error_message: err.response.data,
                    });
                }
            });
    };


    render() {
        const payReceiveGroup_data = this.state.payReceiveGroup_data;

        const payReceiveTransactionFormValid =
            !this.state.pay_receive_group?.length ||
            !this.state.amount?.length ||
            !this.state.remarks?.length ||
            !this.state.transaction_type?.length ||
            !this.state.transaction_date?.length;

        return (
            <Form>
                <Modal show={true} onHide={this.props.onHide} size="lg">
                    <Modal.Header closeButton>
                        <Modal.Title>Pay Receive Transactions</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Row>

                            <Form.Group as={Col} md={6} controlId="formPayReceivetransactionpayReceiveGroup">
                                <Form.Label>Select Person</Form.Label>
                                <Form.Control
                                    as="select"
                                    required
                                    name="pay_receive_group"
                                    onChange={this.onChange}
                                    defaultValue="Chose Person"
                                >
                                    <option>Choose Person</option>
                                    {payReceiveGroup_data && payReceiveGroup_data.filter(data =>
                                        data.total_amount.remain_receivable > 0 || data.total_amount.remain_payable > 0).map((data, index) => (
                                        <option key={data.id} value={data.id} name="group">
                                            {data.name}
                                        </option>
                                    ))}

                                    {/*{payReceiveGroup_data &&*/}
                                    {/*payReceiveGroup_data.map((data, index) => (*/}
                                    {/*    <option key={data.id} value={data.id} name="group">*/}
                                    {/*        {data.name}*/}
                                    {/*    </option>*/}
                                    {/*))}*/}
                                </Form.Control>
                            </Form.Group>

                            <Form.Group as={Col} md={6} controldID='formPayReceivetransactionAmount'>
                                <Form.Label>Amount</Form.Label>
                                <Form.Control
                                    value={this.state.amount}
                                    onChange={this.onChange}
                                    name='amount'
                                    type="number"
                                    setp="0.01"
                                    placeholder="Amount 100"
                                    required
                                    isInvalid={!!this.state.form_error_message.amount}
                                />
                                <Form.Control.Feedback type="invalid">
                                    {this.state.form_error_message.amount}
                                </Form.Control.Feedback>
                            </Form.Group>

                            <Form.Group as={Col} md={6} controlId="formPayReceivetransactionTransactionType">
                                <Form.Label>Transaction Type</Form.Label>
                                <Form.Control
                                    as="select"
                                    required
                                    name="transaction_type"
                                    onChange={this.onChange}
                                    defaultValue="Select Transaction Type"
                                    isInvalid={!!this.state.form_error_message.transaction_type}
                                >
                                    <option>Select Transaction Type</option>
                                    <option value="Pay" name="transaction_type">
                                        Pay
                                    </option>
                                    <option value="Receive" name="transaction_type">
                                        Receive
                                    </option>
                                </Form.Control>

                                <Form.Control.Feedback type="invalid">
                                    {this.state.form_error_message.transaction_type}
                                </Form.Control.Feedback>
                            </Form.Group>

                            <Form.Group as={Col} md={6} controlId="formPayReceivetransactionTransactionDate">
                                <Form.Label>Transaction Date</Form.Label>
                                <Form.Control
                                    value={this.state.transaction_date}
                                    onChange={this.onChange}
                                    type="date"
                                    name="transaction_date"
                                    placeholder="Select Date"
                                    required
                                />
                            </Form.Group>
                        </Form.Row>

                        <Form.Row>
                            <Form.Group as={Col} controlId="formPayReceivetransactionRemakrs">
                                <Form.Label>Remarks</Form.Label>
                                <Form.Control
                                    value={this.state.remarks}
                                    onChange={this.onChange}
                                    name="remarks"
                                    type="text"
                                    placeholder="Paid / Received 1st installment"
                                    required
                                />
                            </Form.Group>
                        </Form.Row>
                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.props.onHide}>
                            Close
                        </Button>
                        <Button
                            variant="primary"
                            type="submit"
                            disabled={payReceiveTransactionFormValid || this.state.is_form_loading}
                            onClick={this.onSubmit}
                        >
                            {this.state.is_form_loading ? "Adding" : "Add"}
                        </Button>
                    </Modal.Footer>
                </Modal>
            </Form>
        );
    }
}

export default PayReceiveTransactionsAdd;