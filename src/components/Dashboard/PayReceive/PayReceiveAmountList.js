import React, { Component } from "react";
import { Button, Spinner, Table } from "react-bootstrap";
import format from "../../../utils/numberFormat";
import PayReceiveAmountEdit from "./PayReceiveAmountEdit";

export class PayReceiveAmountList extends Component {
  state = {
    editPayReceiveAmountID: "",
    payReceiveAmountModalShow: false,
  };

  payReceiveAmountModalClose = () => {
    this.setState({
      payReceiveAmountModalShow: false,
    });
  };
  render() {

    const payReceiveData = this.props.payReceiveAmount_data;
    const is_loading = this.props.is_loading;
    return (
      <>
        <Table>
          <thead>
            <tr className="text-center">
              <th>#</th>
              <th>Person</th>
              <th>Remarks</th>
              <th className="bg-danger text-white">Pay</th>
              <th className="bg-success text-white">Receive</th>
              <th>Actions</th>
            </tr>
          </thead>
          {is_loading && (
            <Spinner animation="border" role="status" variant="primary" />
          )}
          <tbody>
            {payReceiveData &&
              payReceiveData.map((data, index) => (
                <tr key={data.id} className="text-center">
                  <td>{index + 1}</td>
                  <td>{data.group.name}</td>
                  <td>{data.remarks}</td>
                  {data.pay_receive === "Pay" ? (
                    <td>{format.numberFormat(data.amount)}</td>
                    
                  ) : (
                    <td>-</td>
                  )}

                  {data.pay_receive === "Receive" ? (
                    <td>{format.numberFormat(data.amount)}</td>
                  ) : (
                    <td>-</td>
                  )}

                  <td>
                    <Button
                      variant="info"
                      size="sm"
                      className="rounded-0 shadow"
                      onClick={() =>
                        this.setState({
                          editPayReceiveAmountID: data.id,
                          payReceiveAmountModalShow: true,
                        })
                      }
                    >
                      Edit
                    </Button>
                  </td>
                </tr>
              ))}
          </tbody>
        </Table>
        {this.state.payReceiveAmountModalShow && (
          <PayReceiveAmountEdit
            id={this.state.editPayReceiveAmountID}
            onHide={this.payReceiveAmountModalClose}
            getPayReceiveGroupOnAmountEdit={
              this.props.getPayReceiveGroupOnAmountEdit
            }
            getPayReveiveAmountOnAmountEdit={
              this.props.getPayReveiveAmountOnAmountEdit
            }
            payReceiveGroup_data={this.props.payReceiveGroup_data}
          />
        )}
      </>
    );
  }
}

export default PayReceiveAmountList;
