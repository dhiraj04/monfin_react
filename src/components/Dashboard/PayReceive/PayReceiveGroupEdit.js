import React, { Component } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import axiosInstance from "../../../utils/axiosInstance";

export class PayReceiveGroupEdit extends Component {
  state = {
    name: "",
    is_form_loading: false,
  };

  componentDidMount() {
    this.getPayReceiveGroupDetaiilsForEdit();
  }

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  getPayReceiveGroupDetaiilsForEdit() {
    axiosInstance()
    .get(`/pay-receive/edit/${this.props.id}/`)
    .then((res) => {
      this.setState({
        name: res.data.name,
      });
    });
  }

  onSubmit = () => {
    this.setState({ is_form_loading: true });

    let group_data = {
      name: this.state.name,
    };

    axiosInstance()
      .put(`/pay-receive/edit/${this.props.id}/`, group_data)
      .then((res) => {
        this.setState({
          is_form_loading: false,
        });
        this.props.onPayReceiveGroupEdit();
        this.props.onHide();
      })
      .catch((err) => {
        if (err.response) {
          console.log(err.response.data.detail);
          this.setState({
            is_form_loading: false,
          });
        }
      });
  };

  render() {
    return (
      <Form>
        <Modal show={true} onHide={this.props.onHide}>
          <Modal.Header closeButton>
            <Modal.Title>New Group</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group controlId="formUpdatePayReceiveGroup">
              <Form.Label>Name</Form.Label>
              <Form.Control
                value={this.state.name}
                onChange={this.onChange}
                type="text"
                name="name"
                placeholder="Name"
                required
              />
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.props.onHide}>
              Close
            </Button>
            <Button
              variant="primary"
              type="submit"
              disabled={this.state.is_form_loading}
              onClick={this.onSubmit}
            >
              {this.state.is_form_loading ? "Updating" : "Update"}
            </Button>
          </Modal.Footer>
        </Modal>
      </Form>
    );
  }
}

export default PayReceiveGroupEdit;
