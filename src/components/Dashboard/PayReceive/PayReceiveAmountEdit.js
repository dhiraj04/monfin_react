import React, { Component } from "react";
import { Form, Modal, Col, Button } from "react-bootstrap";
import axiosInstance from "../../../utils/axiosInstance";

export class PayReceiveAmountEdit extends Component {
  state = {
    is_form_loading: false,
    group: "",
    amount: "",
    remarks: "",
    pay_receive: "",
    error_message: undefined,
  };

  componentDidMount() {
    this.getFormEditDetails();
  }

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  getFormEditDetails() {
    axiosInstance()
      .get(`pay-receive/amount/edit/${this.props.id}/`)
      .then((res) => {
        this.setState({
          group: res.data.group.id,
          remarks: res.data.remarks,
          amount: res.data.amount,
          pay_receive: res.data.pay_receive,
        });
      });
  }

  onSubmit = (e) => {
    this.setState({ is_form_loading: true });
    let { group, amount, remarks, pay_receive } = this.state;
    let pay_receive_data = {
      group: group,
      amount: amount,
      remarks: remarks,
      pay_receive: pay_receive,
    };

    axiosInstance()
      .put(`/pay-receive/amount/edit/${this.props.id}/`, pay_receive_data)
      .then((res) => {
        this.setState({
          is_form_loading: false,
        });
        this.props.getPayReveiveAmountOnAmountEdit();
        this.props.getPayReceiveGroupOnAmountEdit();
        this.props.onHide();
      })
      .catch((err) => {
        if (err.response) {
          this.setState({
            is_form_loading: false,
            error_message: err.response.data,
          });
        }
      });
  };

  render() {
    const payReceiveGroup_data = this.props.payReceiveGroup_data;
      
    return (
      <Form>
        <Modal show={true} onHide={this.props.onHide} size='lg'>
          <Modal.Header closeButton>
            <Modal.Title>Update Pay Receive</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Row>
              <Form.Group as={Col} controlId="formUpdatePayReceiveAmountGroup">
                <Form.Label>Select Person</Form.Label>
                <Form.Control
                  as="select"
                  required
                  name="group"
                  onChange={this.onChange}
                  value={this.state.group}
                >
                  <option>Choose Person</option>
                  {payReceiveGroup_data &&
                    payReceiveGroup_data.map((data, index) => (
                      <option key={data.id} value={data.id} name="group">
                        {data.name}
                      </option>
                    ))}
                </Form.Control>
              </Form.Group>

              <Form.Group as={Col} md="3" controlId="formUpdatePayReceiveAmountamount">
                <Form.Label>Amount</Form.Label>
                <Form.Control
                  value={this.state.amount}
                  onChange={this.onChange}
                  name="amount"
                  type="number"
                  setp="0.01"
                  placeholder="Amount 100"
                  required
                />
              </Form.Group>

              <Form.Group as={Col} controlId="formUpdatePayReceiveAmountPayReceive">
                <Form.Label>PayReceive</Form.Label>
                <Form.Control
                  as="select"
                  required
                  name="pay_receive"
                  onChange={this.onChange}
                  value={this.state.pay_receive}
                >
                  <option>Select Pay or Receive</option>
                  <option value="Pay" name="pay_reveive">
                    Pay
                  </option>
                  <option value="Receive" name="pay_reveive">
                    Receive
                  </option>
                </Form.Control>
              </Form.Group>
            </Form.Row>

            <Form.Row>
              <Form.Group as={Col} controlId="formUpdatePayReceiveAmountremarks">
                <Form.Label>Remarks</Form.Label>
                <Form.Control
                  value={this.state.remarks}
                  onChange={this.onChange}
                  name="remarks"
                  type="text"
                  placeholder="Whatever"
                  required
                />
              </Form.Group>
            </Form.Row>
          </Modal.Body>

          <Modal.Footer>
            <Button variant="secondary" onClick={this.props.onHide}>
              Close
            </Button>
            <Button
              variant="primary"
              type="submit"
              disabled={this.state.is_form_loading}
              onClick={this.onSubmit}
            >
              {this.state.is_form_loading ? "Updating" : "Update"}
            </Button>
          </Modal.Footer>
        </Modal>
      </Form>
    );
  }
}

export default PayReceiveAmountEdit;
