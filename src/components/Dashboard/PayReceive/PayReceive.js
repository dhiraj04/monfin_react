import fileDownload from "js-file-download";
import React, { Component } from "react";
import { Card, Button, Container, Row, Col, Form, Pagination } from "react-bootstrap";
import axiosInstance from "../../../utils/axiosInstance";
import Topnav from "../../Navbar/Topnav";
import PayReceiveAmountAdd from "./PayReceiveAmountAdd";
import PayReceiveAmountList from "./PayReceiveAmountList";
import PayReceiveGroupAdd from "./PayReceiveGroupAdd";
import PayReceiveList from "./PayReceiveList";
import PayReceiveTransactionsList from "./PayReceiveTransactionsList";

export class PayReceive extends Component {
  state = {
    payReceiveGroup_is_loading: false,
    payReceiveGroup_data: "",
    payReceiveModalShow: false,

    payReceiveAmount_data: "",
    payReceiveAmount_is_loading: false,
    payReceiveAmountModalShow: false,

    search: "",

    currentPage: 1,
    postPerPage: 10,
    totalPosts: "",
  };

  componentDidMount() {
    this.getPayReceiveAmountList(1);
    this.getPayReceiveGroupList();
  }

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  // pdf and excel report function for PayReceiveAmountList
  getpdffile(report) {
    axiosInstance()
        .get(
            `/pay-receive/amount/?group__name=${this.state.search}&report=${report}`,
            {
              responseType: "blob",
            }
        )
        .then((res) => {
          if (report === "pdf") {
            fileDownload(res.data, "report.pdf");
          } else if (report === "excel") {
            fileDownload(res.data, "report.xlsx");
          }
        });
  }

  searchPayReceiveAmountList() {
    axiosInstance()
        .get(
            `/pay-receive/amount/?group__name=${this.state.search}`
        )
        .then((res) => {
          this.setState({
            payReceiveAmount_data: res.data.results,
            totalPosts: res.data.count,
            currentPage: res.data.page,
          });
        });
  }

  getPayReceiveAmountList(pageNumber) {
    axiosInstance()
        .get(
            `/pay-receive/amount/?page=${pageNumber}&group__name=${this.state.search}`
        )
        .then((res) => {
          this.setState({
            payReceiveAmount_data: res.data.results,
            totalPosts: res.data.count,
            currentPage: res.data.page,
          });
        });
  }

  getPayReceiveGroupList() {
    axiosInstance()
        .get("/pay-receive/")
        .then((res) => {
          this.setState({ payReceiveGroup_data: res.data });
        });
  }

  payReceiveModalClose = () => {
    this.setState({
      payReceiveModalShow: false,
    });
  };

  payReceiveAmountModalClose = () => {
    this.setState({
      payReceiveAmountModalShow: false,
    });
  };

  render() {
    const total_pay_amount = Array.from(this.state.payReceiveGroup_data).map(data => data.total_amount.remain_payable);
    const total_receive_amount = Array.from(this.state.payReceiveGroup_data).map(data => data.total_amount.remain_receivable);

    const total_pay_amount_sum = total_pay_amount.reduce((a,b) => a+b, 0);
    const total_receive_amount_sum = total_receive_amount.reduce((a,b) => a+b, 0);

    const pageNumbers = [];

    for (
        let i = 1;
        i <= Math.ceil(this.state.totalPosts / this.state.postPerPage);
        i++
    ) {
      pageNumbers.push(i);
    }

    const renderPageNumbers = pageNumbers.map((number) => {
      return (
          <Pagination.Item
              key={number}
              onClick={() => this.getPayReceiveAmountList(number)}
              active={number === this.state.currentPage}
          >
            {number}
          </Pagination.Item>
      );
    });

    return (
        <>
          <Topnav />
          <Container fluid className="px-4">
            <Row className="mt-3 mb-3">
              <Col md={3}>
                <h5>Payables || Receivables</h5>
              </Col>
              <Col md={3}>
                <Button
                    className="float-right rounded-0 shadow"
                    variant="warning"
                    onClick={() =>
                        this.setState({
                          payReceiveAmountModalShow: true,
                        })
                    }
                >
                  Add PayReceive
                </Button>

                {this.state.payReceiveAmountModalShow && (
                    <PayReceiveAmountAdd
                        onHide={this.payReceiveAmountModalClose}
                        onPayReceiveAmountAdd={() => this.getPayReceiveAmountList(1)}
                        getAccountGroup={() => this.getPayReceiveGroupList()}
                        payReceiveGroup_data={this.state.payReceiveGroup_data}
                    />
                )}
              </Col>
              <Col md={6}>
                <Button
                    className="float-right rounded-0 shadow"
                    variant="warning"
                    onClick={() =>
                        this.setState({
                          payReceiveModalShow: true,
                        })
                    }
                >
                  Add Person
                </Button>
                {this.state.payReceiveModalShow && (
                    <PayReceiveGroupAdd
                        onHide={this.payReceiveModalClose}
                        onPayReceiveGroupAdd={() => this.getPayReceiveGroupList()}
                    />
                )}
              </Col>
            </Row>

            <Row>
              <Col>
                <Card className='shadow rounded-0'>
                  <Card.Header>
                    <Card.Title>
                      <h6>Pay Receive List</h6>
                      <Button
                          className="shadow rounded-0 m-1"
                          variant="primary"
                          onClick={() => this.getpdffile("pdf")}
                      >
                        PDF
                      </Button>
                      <Button
                          className="shadow rounded-0"
                          variant="primary"
                          onClick={() => this.getpdffile("excel")}
                      >
                        Excel
                      </Button>
                      <Form inline className="float-right">
                        <Form.Control
                            type="text"
                            placeholder="Search"
                            className="mr-sm-2"
                            name="search"
                            value={this.state.search}
                            onChange={this.onChange}
                        />
                        <Button
                            variant="outline-success"
                            onClick={() => this.searchPayReceiveAmountList()}
                        >
                          Search
                        </Button>
                      </Form>
                    </Card.Title>
                  </Card.Header>
                  <Card.Body>
                    <PayReceiveAmountList
                        payReceiveGroup_data={this.state.payReceiveGroup_data}
                        payReceiveAmount_data={this.state.payReceiveAmount_data}
                        is_loading={this.state.payReceiveAmount_is_loading}
                        getPayReceiveGroupOnAmountEdit={() =>
                            this.getPayReceiveGroupList()
                        }
                        getPayReveiveAmountOnAmountEdit={() =>
                            this.getPayReceiveAmountList(1)
                        }
                    />
                    <Pagination>
                      {renderPageNumbers}
                    </Pagination>
                  </Card.Body>
                </Card>
              </Col>

              <Col>
                <Card className='shadow rounded-0'>
                  <Card.Header>
                    <Card.Title> <h6>Groups</h6></Card.Title>
                  </Card.Header>
                  <Card.Body>
                    <PayReceiveList
                        total_pay_amount={total_pay_amount_sum}
                        total_receive_amount={total_receive_amount_sum}
                        payReceiveGroup_data={this.state.payReceiveGroup_data}
                        is_laoding={this.state.payReceiveGroup_is_loading}
                        onPayReceiveEdit={() => this.getPayReceiveGroupList()}
                    />
                  </Card.Body>
                </Card>
              </Col>
            </Row>

            <Row className='my-5'>
              <Col>
                <Card className='shadow rounded-0'>
                  <Card.Body>
                    <PayReceiveTransactionsList
                        onPayreceiveTransactionAdd_getGroup={() => this.getPayReceiveGroupList()}
                    />
                  </Card.Body>
                </Card>

              </Col>
            </Row>
          </Container>
        </>
    );
  }
}

export default PayReceive;
