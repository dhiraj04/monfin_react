import React, { Component } from "react";
import { Button, Card, Col, Container, Row } from "react-bootstrap";
import Topnav from "../../Navbar/Topnav";
import {
  FaTransgenderAlt,
  FaAddressCard,
  FaBirthdayCake,
  FaMobileAlt,
  FaPencilAlt,
} from "react-icons/fa";

import { MdEmail } from "react-icons/md";
import ProfileUpdate from "./ProfileUpdate";
import axiosInstance from "../../../utils/axiosInstance";
import ChangePassword from "./ChangePassword";

class Profile extends Component {
  state = {
    profile_data: "",
    profileModalShow: false,

    passwordChangeModalShow: false,
  };

  componentDidMount() {
    this.getUserProfile();
  }

  getUserProfile() {
    axiosInstance()
        .get("/consumers/profile/")
        .then((res) => {
          this.setState({
            profile_data: res.data,
          });
        });
  }

  handleModalClose = () => {
    this.setState({ profileModalShow: false });
  };

  handlePasswordChangeModalClose = () => {
    this.setState({ passwordChangeModalShow: false });
  };

  render() {
    const profile = this.state.profile_data;

    return (
        <>
          <Topnav />
          <Container fluid>
            <Row className="p-3">
              <Col md={{ span: 10, offset: 1 }}>
                <h3>Account Details</h3>
              </Col>
            </Row>

            <Row className='mb-3'>
              <Col md={{ span: 10, offset: 1 }}>
                <Button size={'sm'} className='float-right rounded-0 border-0 shadow'  onClick={() =>
                    this.setState({ passwordChangeModalShow: true })
                }>
                  Change Password
                </Button>

                {this.state.passwordChangeModalShow && (
                  <ChangePassword onHide={this.handlePasswordChangeModalClose}/>
                )}
              </Col>
            </Row>

            <Row>
              <Col md={{ span: 10, offset: 1 }}>
                <Card className="shadow">
                  <Card.Body>
                    <Row className="p-4">
                      <Col md={{ span: 6, offset: 3 }}>
                        <h2 className="text-center">
                          {profile && profile.full_name}{" "}
                          <Button
                              variant="warning"
                              className="border-0 rounded-0 mb-1"
                              onClick={() =>
                                  this.setState({ profileModalShow: true })
                              }
                          >
                            <FaPencilAlt size={18} /> Edit
                          </Button>
                        </h2>

                        {this.state.profileModalShow && (
                            <ProfileUpdate
                                onHide={this.handleModalClose}
                                onProfileUpdate={() => this.getUserProfile()}
                            />
                        )}
                      </Col>
                    </Row>

                    <Row>
                      <Col className="text-center">
                        <h5>{profile && profile.email}</h5>
                        <span>
                        <MdEmail size={24} />
                      </span>
                      </Col>
                    </Row>

                    <Row className="mt-5 mb-5">
                      <Col md={3}>
                        <Card className="shadow text-center">
                          <Card.Body>
                            <h5>{profile && profile.gender}</h5>
                            <span>
                            <FaTransgenderAlt size={24} />
                          </span>
                          </Card.Body>
                        </Card>
                      </Col>

                      <Col md={3}>
                        <Card className="shadow text-center">
                          <Card.Body>
                            <h5>{profile && profile.address}</h5>
                            <span>
                            <FaAddressCard size={24} />
                          </span>
                          </Card.Body>
                        </Card>
                      </Col>

                      <Col md={3}>
                        <Card className="shadow text-center">
                          <Card.Body>
                            <h5>{profile && profile.dob}</h5>
                            <span>
                            <FaBirthdayCake size={24} />
                          </span>
                          </Card.Body>
                        </Card>
                      </Col>

                      <Col md={3}>
                        <Card className="shadow text-center">
                          <Card.Body>
                            <h5>{profile && profile.mobile}</h5>
                            <span>
                            <FaMobileAlt size={24} />
                          </span>
                          </Card.Body>
                        </Card>
                      </Col>
                    </Row>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
          </Container>
        </>
    );
  }
}

export default Profile;
