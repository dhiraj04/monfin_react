import React, { Component } from "react";
import { Button, Col, Form, Modal } from "react-bootstrap";
import axiosInstance from "../../../utils/axiosInstance";

export default class ProfileUpdate extends Component {
  state = {
    is_form_loading: false,
    first_name: "",
    last_name: "",
    gender: "",
    dob: "",
    email: "",
    address: "",
    mobile: "",
    error_message: '',
  };

  componentDidMount() {
    this.getFormEditData();
  }

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  getFormEditData() {
    axiosInstance()
        .get(`/consumers/profile/`)
        .then((res) => {
          this.setState({
            first_name: res.data.first_name,
            last_name: res.data.last_name,
            gender: res.data.gender,
            dob: res.data.dob,
            email: res.data.email,
            address: res.data.address,
            mobile: res.data.mobile,
          });
        });
    // .catch((err) => {
    //   if (err.response) {
    //     this.setState({is_form_loading: false, error_message: err.response.data})
    //   }
    // });
  }

  onSubmit = () => {
    this.setState({ is_form_loading: true });
    let {
      first_name,
      last_name,
      gender,
      dob,
      email,
      address,
      mobile,
    } = this.state;

    let profile_data = {
      first_name: first_name,
      last_name: last_name,
      gender: gender,
      dob: dob,
      email: email,
      address: address,
      mobile: mobile,
    };

    axiosInstance()
        .put(`/consumers/profile/edit/`, profile_data)
        .then((res) => {
          this.setState({ is_form_loading: false });
          this.props.onProfileUpdate();
          this.props.onHide();
        })
        .catch((err) => {
          if (err.response) {
            this.setState({is_form_loading: false, error_message: err.response.data})
          }
        });
  };

  render() {
    const editProfileFormValid =
        !this.state.first_name?.length ||
        !this.state.last_name?.length ||
        !this.state.gender?.length ||
        !this.state.dob?.length ||
        !this.state.email?.length ||
        !this.state.address?.length ||
        !this.state.mobile?.length;

    return (
        <Form>
          <Modal show={true} size="lg" onHide={this.props.onHide}>
            <Modal.Header closeButton>
              <Modal.Title>Edit Profile {this.state.group_name}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form.Row>
                <Form.Group as={Col} controlId="formProfileFirstName">
                  <Form.Label>First Name</Form.Label>
                  <Form.Control
                      type="text"
                      placeholder="First Name"
                      value={this.state.first_name}
                      name="first_name"
                      onChange={this.onChange}
                      required
                  />
                </Form.Group>

                <Form.Group as={Col} controlId="formProfileLastName">
                  <Form.Label>Last Name</Form.Label>
                  <Form.Control
                      type="text"
                      placeholder="Last Name"
                      value={this.state.last_name}
                      name="last_name"
                      onChange={this.onChange}
                      required
                  />
                </Form.Group>

                <Form.Group as={Col} controlId="formProfileEmail">
                  <Form.Label>Email</Form.Label>
                  <Form.Control
                      type="email"
                      placeholder="Email"
                      value={this.state.email}
                      name="email"
                      onChange={this.onChange}
                      required
                      isInvalid={!!this.state.error_message.email}
                  />
                  <Form.Control.Feedback type="invalid">
                    {this.state.error_message.email}
                  </Form.Control.Feedback>
                </Form.Group>
              </Form.Row>

              <Form.Group controlId="formProfileAddress">
                <Form.Label>Address</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Address"
                    value={this.state.address}
                    name="address"
                    onChange={this.onChange}
                    required
                />
              </Form.Group>

              <Form.Row>
                <Form.Group as={Col} controlId="formProfileGender">
                  <Form.Label>Gender</Form.Label>
                  <Form.Control
                      as="select"
                      required
                      name="gender"
                      onChange={this.onChange}
                      value={this.state.gender}
                  >
                    <option>Select Gender</option>
                    <option value="Male" name="gender">
                      Male
                    </option>
                    <option value="Female" name="gender">
                      Female
                    </option>
                    <option value="Other" name="gender">
                      Other
                    </option>
                  </Form.Control>
                </Form.Group>

                <Form.Group as={Col} md="4" controlId="formProfileDob">
                  <Form.Label>Date of Birth</Form.Label>
                  <Form.Control
                      value={this.state.dob}
                      onChange={this.onChange}
                      type="date"
                      name="dob"
                      placeholder="Select DOB"
                      required
                  />
                </Form.Group>

                <Form.Group as={Col} md="4" controlId="formProfileMobile">
                  <Form.Label>Mobile</Form.Label>
                  <Form.Control
                      value={this.state.mobile}
                      onChange={this.onChange}
                      type="number"
                      name="mobile"
                      placeholder="Mobile No."
                      required
                      isInvalid={!!this.state.error_message.mobile}
                  />
                  <Form.Text className="text-muted">
                    10 digits maximum
                  </Form.Text>
                  <Form.Control.Feedback type="invalid">
                    {this.state.error_message.mobile}
                  </Form.Control.Feedback>
                </Form.Group>
              </Form.Row>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={this.props.onHide}>
                Close
              </Button>
              <Button
                  variant="primary"
                  type="submit"
                  disabled={editProfileFormValid || this.state.is_form_loading}
                  onClick={this.onSubmit}
              >
                {this.state.is_form_loading ? "Updating" : "Update"}
              </Button>
            </Modal.Footer>
          </Modal>
        </Form>
    );
  }
}
