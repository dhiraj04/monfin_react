import React, {Component} from 'react';
import {Button, Form, Modal} from 'react-bootstrap';
import axiosInstance from "../../../utils/axiosInstance";
import {history} from "../../../history";

class ChangePassword extends Component {
    state = {
        password: '',
        new_password: "",
        confirm_password: '',

        is_form_loading: false,
        form_error_message: '',
    };

    onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    };


    onSubmit = () => {
        this.setState({ is_form_loading: true });
        let {
            password,
            new_password,
            confirm_password,
        } = this.state;

        let password_data = {
            password: password,
            new_password: new_password,
            confirm_password: confirm_password,
        };

        axiosInstance()
            .put(`/user/change-password/`, password_data)
            .then((res) => {
                this.setState({ is_form_loading: false });
                this.props.onHide();
                localStorage.removeItem('token');
                history.push("/login");
            })
            .catch((err) => {
                if (err.response) {
                    this.setState({is_form_loading: false, form_error_message: err.response.data})
                }
            });
    };

    render() {

        const passwordFormValid =
            !this.state.password?.length ||
            !this.state.new_password?.length ||
            !this.state.confirm_password?.length ;

        return (
            <Form>
                <Modal show={true} onHide={this.props.onHide}>
                    <Modal.Header closeButton>
                        <Modal.Title>Change Password</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group>
                            <Form.Label>Old Password</Form.Label>
                            <Form.Control
                                type="password"
                                placeholder="Old Password"
                                value={this.state.password}
                                name="password"
                                onChange={this.onChange}
                                required
                                isInvalid={!!this.state.form_error_message.password}
                            />
                            <Form.Control.Feedback type="invalid">
                                {this.state.form_error_message.password}
                            </Form.Control.Feedback>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>New Password</Form.Label>
                            <Form.Control
                                type="password"
                                placeholder="New Password"
                                value={this.state.new_password}
                                name="new_password"
                                onChange={this.onChange}
                                required
                                isInvalid={!!this.state.form_error_message.new_password}
                            />
                            <Form.Text className="text-muted">
                                min. 8 Characters
                            </Form.Text>
                            <Form.Control.Feedback type="invalid">
                                {this.state.form_error_message.password}
                            </Form.Control.Feedback>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Confirm New Password</Form.Label>
                            <Form.Control
                                type="password"
                                placeholder="Confirm New Password"
                                value={this.state.confirm_password}
                                name="confirm_password"
                                onChange={this.onChange}
                                required
                                isInvalid={!!this.state.form_error_message.confirm_password}
                            />
                            <Form.Control.Feedback type="invalid">
                                {this.state.form_error_message.confirm_password}
                            </Form.Control.Feedback>
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Form.Text className='text-muted'>"You will be logged out after password change"</Form.Text>
                        <Button variant="secondary" onClick={this.props.onHide}>
                            Close
                        </Button>
                        <Button
                            variant="primary"
                            type="submit"
                            disabled={passwordFormValid || this.state.is_form_loading}
                            onClick={this.onSubmit}
                        >
                            {this.state.is_form_loading ? "Updating" : "Update"}
                        </Button>
                    </Modal.Footer>
                </Modal>
            </Form>
        );
    }
}

export default ChangePassword;