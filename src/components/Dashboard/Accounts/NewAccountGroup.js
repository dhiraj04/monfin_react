import React, { Component } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import axiosInstance from "../../../utils/axiosInstance";

class NewAccountGroup extends Component {
  state = {
    is_form_loading: false,
    group_name: "",
    error_message: undefined,
  };

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = (e) => {
    this.setState({ is_form_loading: true });
    let { group_name } = this.state;
    let group_data = {
      group_name: group_name,
    };

    axiosInstance()
      .post("/accounts/groups/add/", group_data)
      .then((res) => {
        this.props.onAccountGroupAdd();
        this.setState({ 
          is_form_loading: false,
        });
      });
      this.props.onHide();
  };

  render() {
    const accountGroupFormValid = !this.state.group_name?.length;

    return (
      <Form>
        <Modal
          show={true}
          onHide={this.props.onHide}
        >
          <Modal.Header closeButton>
            <Modal.Title>Add New Account Group</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group controlId="formAccountGroupName">
              <Form.Label>Account Group Name</Form.Label>
              <Form.Control
                value={this.state.group_name}
                onChange={this.onChange}
                name="group_name"
                type="text"
                placeholder="Group Name"
                required
              />
            </Form.Group>
          </Modal.Body>

          <Modal.Footer>
            <Button
              variant="secondary"
              onClick={this.props.onHide}
            >
              Close
            </Button>
            <Button
              variant="primary"
              type="submit"
              disabled={accountGroupFormValid || this.state.is_form_loading}
              onClick={this.onSubmit}
            >
              {this.state.is_form_loading ? "Adding New Account Gorup" : "Add"}
            </Button>
          </Modal.Footer>
        </Modal>
      </Form>
    );
  }
}

// const mapDispatchToProps = {
//   accountGroupDataSuccess,
// };

// export default connect(null, mapDispatchToProps)(withRouter(NewAccountGroup));

export default NewAccountGroup;
