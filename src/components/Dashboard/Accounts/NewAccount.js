import React, { Component } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import { connect } from "react-redux";
import { totalAmountData } from "../../../features/userSlice/TotalAmountSlice";
import apicalls from "../../../utils/apiCalls";
import axiosInstance from "../../../utils/axiosInstance";

class NewAccount extends Component {
  state = {
    is_form_loading: false,
    account_name: "",
    amount: "",
    account_group: "",
    account_groups: "",
    error_message: undefined,
  };

  componentDidMount() {
    this.getAccountGroup();
  }

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  getAccountGroup() {
    axiosInstance(this.props.history)
      .get("/accounts/groups/")
      .then((res) => {
        this.setState({
          account_groups: res.data,
        });
      })
      .catch((err) => {
        if (err.response) {
          console.log(err.response.data.details);
        }
      });
  }

  onSubmit = (e) => {
    this.setState({ is_form_loading: true });
    let { account_name, amount, account_group } = this.state;
    let account_data = {
      account_name: account_name,
      amount: amount,
      account_group: account_group,
    };

    axiosInstance()
      .post("/accounts/add/", account_data)
      .then((res) => {
        this.props.onAccountAdd();
        this.setState({
          is_form_loading: false,
        });
        apicalls.getTotalAmount(this.props.totalAmountData);
        this.props.onHide();
      });
  };

  render() {
    const account_groups = this.state.account_groups;
    const accountFormValid =
      !this.state.account_name?.length || !this.state.account_group?.length;

    return (
      <Form>
        <Modal show={true} onHide={this.props.onHide}>
          <Modal.Header closeButton>
            <Modal.Title>Add New Account</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group controlId="formAccountName">
              <Form.Label>Account Name</Form.Label>
              <Form.Control
                value={this.state.account_name}
                onChange={this.onChange}
                name="account_name"
                type="text"
                placeholder="Account Name"
                required
              />
            </Form.Group>

            <Form.Group controlId="formamount">
              <Form.Label>Amount</Form.Label>
              <Form.Control
                value={this.state.amount}
                onChange={this.onChange}
                name="amount"
                type="number"
                placeholder="Amount 100"
                required
              />
            </Form.Group>

            <Form.Group controlId="formAccountGroup.ControlSelect2">
              <Form.Label>Account Group</Form.Label>
              <Form.Control
                as="select"
                required
                name="account_group"
                onChange={this.onChange}
                defaultValue="Choose Account Group"
              >
                <option>Choose Account Group</option>
                {account_groups &&
                  account_groups.map((account_group, index) => (
                    <option
                      key={account_group.id}
                      value={account_group.id}
                      name="group_name"
                    >
                      {account_group.group_name}
                    </option>
                  ))}
              </Form.Control>
            </Form.Group>
          </Modal.Body>

          <Modal.Footer>
            <Button variant="secondary" onClick={this.props.onHide}>
              Close
            </Button>
            <Button
              variant="primary"
              type="submit"
              disabled={accountFormValid || this.state.is_form_loading}
              onClick={this.onSubmit}
            >
              Add {this.state.is_form_loading ? "Adding New Account" : "Add"}
            </Button>
          </Modal.Footer>
        </Modal>
      </Form>
    );
  }
}

const mapDispatchToProps = {
  totalAmountData,
};

export default connect(null, mapDispatchToProps)(NewAccount);
