import React, { Component } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import axiosInstance from "../../../utils/axiosInstance";

class EditAccountGroup extends Component {
  state = {
    is_form_loading: false,
    group_name: "",
    error_message: undefined,
  };

  componentDidMount() {
    this.getAccountGroupFormData();
  }

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  getAccountGroupFormData() {
    axiosInstance()
      .get(`/accounts/groups/edit/${this.props.id}/`)
      .then((res) => {
        this.setState({
          group_name: res.data.group_name,
        });
      })
      .catch((err) => {
        if (err.response) {
          console.log(err.response.data.details);
        }
      });
  }

  onSubmit = () => {
    this.setState({ is_form_loading: true });
    let { group_name } = this.state;
    let group_data = {
      group_name: group_name,
    };

    axiosInstance()
      .put(`/accounts/groups/edit/${this.props.id}/`, group_data)
      .then((res) => {
        this.props.onAccountGroupEdit();
        this.setState({ is_form_loading: false });
        this.props.onHide();
      })
      .catch((err) => {
        this.setState({
          is_form_loading: false,
          error_message: err.response.data,
        });
      });
  };

  render() {

    const editAccountGroupFormValid = !this.state.group_name?.length;

    return (
      <Form>
        <Modal
          show={true}
          onHide={this.props.onHide}
        >
          <Modal.Header closeButton>
            <Modal.Title>Edit Account Group {this.state.group_name}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group controlId="formAccountGroupName">
              <Form.Label>Account Group Name</Form.Label>
              <Form.Control
                value={this.state.group_name}
                onChange={this.onChange}
                name="group_name"
                type="text"
                placeholder="Group Name"
                required
              />
            </Form.Group>
          </Modal.Body>

          <Modal.Footer>
            <Button
              variant="secondary"
              onClick={this.props.onHide}
            >
              Close
            </Button>
            <Button
              variant="primary"
              type="submit"
              disabled={editAccountGroupFormValid || this.state.is_form_loading}
              onClick={this.onSubmit}
            >
              {this.state.is_form_loading ? "Updating Account Gorup" : "Update"}
            </Button>
          </Modal.Footer>
        </Modal>
      </Form>
    );
  }
}

// const mapDispatchToProps = {
//   accountGroupEditDataSuccess,
// };

// export default connect(null, mapDispatchToProps)(withRouter(EditAccountGroup));

export default EditAccountGroup;
