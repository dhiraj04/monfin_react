import React, { Component } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { accountEditDataSuccess } from "../../../features/userSlice/accoutSlice";
import axiosInstance from "../../../utils/axiosInstance";

class NewAccount extends Component {
  state = {
    is_form_loading: false,
    account_name: "",
    account_group: "",
    error_message: undefined,

    account_groups: "",
  };

  componentDidMount() {
    this.getAccountGroup();
    this.get_account_edit_form();
  }

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  getAccountGroup() {
    axiosInstance()
      .get("/accounts/groups/")
      .then((res) => {
        this.setState({
          account_groups: res.data,
        });
      })
      .catch((err) => {
        if (err.response) {
          console.log(err.response.data.details);
        }
      });
  }

  get_account_edit_form = () => {
    axiosInstance()
      .get(`/accounts/edit/${this.props.id}/`)
      .then((res) => {
        let data = res.data;
        let account_group = data.account_group.id;

        this.setState({
          account_name: data.account_name,
          account_group: account_group,
        });
      })
      .catch((err) => {
        if (err.response) {
          console.log(err.response.data.detail);
        }
      });
  };

  onSubmit = () => {
    this.setState({ is_form_loading: true });
    let { account_name, account_group } = this.state;
    let account_data = {
      account_name: account_name,
      account_group: account_group,
    };

    axiosInstance()
      .put(`/accounts/edit/${this.props.id}/`, account_data)
      .then((res) => {
        this.props.onAccountEdit();
        this.setState({
          is_form_loading: false,
        });
        this.props.onHide();
      });
  };

  render() {
    const account_groups = this.state.account_groups;

    return (
      <Form>
        <Modal
          show={true}
          onHide={this.props.onHide}
        >
          <Modal.Header closeButton>
            <Modal.Title>Edit Account</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group controlId="formAccountName">
              <Form.Label>Account Name</Form.Label>
              <Form.Control
                value={this.state.account_name}
                onChange={this.onChange}
                name="account_name"
                type="text"
                placeholder="Account Name"
                required
              />
            </Form.Group>

            <Form.Group controlId="formAccountGroup.ControlSelect2">
              <Form.Label>Account Group</Form.Label>
              <Form.Control
                as="select"
                required
                name="account_group"
                onChange={this.onChange}
                value={this.state.account_group}
              >
                {account_groups &&
                  account_groups.map((account_group, index) => (
                    <option
                      key={account_group.id}
                      value={account_group.id}
                      name="group_name"
                    >
                      {account_group.group_name}
                    </option>
                  ))}
              </Form.Control>
            </Form.Group>
          </Modal.Body>

          <Modal.Footer>
            <Button
              variant="secondary"
              onClick={this.props.onHide}
            >
              Close
            </Button>
            <Button
              variant="primary"
              type="submit"
              disabled={this.state.is_form_loading}
              onClick={this.onSubmit}
            >
              {this.state.is_form_loading ? "Updating Account" : "Update"}
            </Button>
          </Modal.Footer>
        </Modal>
      </Form>
    );
  }
}

const mapDispatchToProps = {
  accountEditDataSuccess,
};

export default connect(null, mapDispatchToProps)(withRouter(NewAccount));
