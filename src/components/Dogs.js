import axios from "axios";
import React, { Component } from "react";
import { Button, Card, Col, Container, Row } from "react-bootstrap";

export default class Dogs extends Component {
  state = {
    dog_is_loading: false,
    cat_is_loading: false,
    dog_data: "",
    cat_data: "",
    error_message: undefined,

    joke_data: "",
    joke_is_loading: false,
  };

  componentDidMount() {
    this.getDog();
    this.getCat();
    this.getJoke();
  }

  getDog() {
    this.setState({
      dog_is_loading: true,
    });
    axios.get("https://dog.ceo/api/breeds/image/random").then((res) => {
      this.setState({
        dog_data: res.data,
        dog_is_loading: false,
      });
    });
  }

  getCat() {
    this.setState({
      cat_is_loading: true,
    });
    axios.get("http://aws.random.cat/meow").then((res) => {
      this.setState({
        cat_data: res.data,
        cat_is_loading: false,
      });
    });
  }

  getJoke() {
    this.setState({
      joke_is_loading: true,
    });
    axios
      .get("https://v2.jokeapi.dev/joke/Any?blacklistFlags=nsfw&type=single")
      .then((res) => {
        this.setState({
          joke_data: res.data,
          joke_is_loading: false,
        });
      });
  }

  render() {
    const dog = this.state.dog_data;
    const cat = this.state.cat_data;
    const joke = this.state.joke_data;

    return (
      <Container fluid>
        <Row className="p-5">
          <Col>
            <Card className="shadow">
              <Card.Body>
                {this.state.dog_is_loading && <h1>Dog is on the way</h1>}
                {dog && (
                  <img height="500" width="500" src={dog.message} alt="dog" />
                )}
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card className="shadow">
              <Card.Body>
                {this.state.cat_is_loading && <h1>Cat is on the way</h1>}

                {cat && (
                  <img height="500" width="500" src={cat.file} alt="Cat" />
                )}
              </Card.Body>
            </Card>
          </Col>
        </Row>

        <Row className='p-5'>
          <Col>
            <Card className="shadow">
              <Card.Body>{joke && joke.joke}</Card.Body>
            </Card>
          </Col>
        </Row>

        <Button
          variant="primary"
          onClick={() => {
            this.getDog();
            this.getCat();
            this.getJoke();
          }}
          disabled={this.state.cat_is_loading || this.state.dog_is_loading}
        >
          {this.state.cat_is_loading || this.state.dog_is_loading
            ? "Getting dog and cats "
            : "Get"}
        </Button>
      </Container>
    );
  }
}
