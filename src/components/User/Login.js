import React, { Component } from "react";
import {
  Button,
  Col,
  Container,
  Form,
  Row,
  Card,
  Alert,
} from "react-bootstrap";
import { connect } from "react-redux";
import { loginSuccess } from "../../features/userSlice/userSlice";
import { history } from "../../history";
import axiosInstance from "../../utils/axiosInstance";
import isAuthenticated from "../../utils/isAuthenticated";
import Register from "./Register";

class Login extends Component {
  state = {
    is_form_loading: false,
    error_message: undefined,
    username: "",
    password: "",

    registerModalShow: false,
  };

  componentDidMount() {
    this.checkLoginStatus();
  }

  checkLoginStatus() {
    if (isAuthenticated()) {
      history.push("/dashboard");
    }
  }

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = (e) => {
    e.preventDefault();
    this.setState({ is_form_loading: true });
    let { username, password } = this.state;
    let credentials = {
      username: username,
      password: password,
    };

    axiosInstance()
      .post("/user/login/auth/", credentials)
      .then((res) => {
        localStorage.setItem("token", res.data.token);
        this.setState({ is_form_loading: false });
        this.props.loginSuccess(res.data);
        history.push("/dashboard");
      })
      .catch((err) => {
        if (err.response) {
          this.setState({ is_form_loading: false });
          this.setState({ error_message: err.response.data });
        }
      });
  };

  handleRegisterModalClose = () => {
    this.setState({
      registerModalShow: false,
    });
  };

  render() {
    const LoginFormValid =
      !this.state.username?.length || !this.state.password?.length;

    return (
      <Container fluid style={{ marginTop: 300 }}>
        <Row className="justify-content-md-center">
          <Col md={3}>
            <Card className="rounded-0 shadow">
              <Card.Body>
                <Card.Title className="text-center">Login</Card.Title>
                <Form>
                  {this.state.error_message && (
                    <Alert variant="danger">
                      {this.state.error_message?.details}
                    </Alert>
                  )}

                  <Form.Group controlId="formBasicUsername">
                    <Form.Label>Username</Form.Label>

                    <Form.Control
                      value={this.state.username}
                      onChange={this.onChange}
                      name="username"
                      type="text"
                      placeholder="Username"
                    />
                  </Form.Group>

                  <Form.Group controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>

                    <Form.Control
                      value={this.state.password}
                      onChange={this.onChange}
                      name="password"
                      type="password"
                      placeholder="Password"
                    />
                  </Form.Group>

                  <Button
                    onClick={this.onSubmit}
                    disabled={LoginFormValid || this.state.is_form_loading}
                    variant="primary flat"
                    type="submit"
                    block
                  >
                    {this.state.is_form_loading ? "Logging In" : "Login"}
                  </Button>
                </Form>
              </Card.Body>
            </Card>
          </Col>
        </Row>

        <Row className="justify-content-md-center mt-3">
          <Col md={3}>
            <Button
              variant="warning"
              onClick={() => this.setState({ registerModalShow: true })}
            >
              Register Now
            </Button>
            {this.state.registerModalShow && (
              <Register onHide={this.handleRegisterModalClose} />
            )}
          </Col>
        </Row>
      </Container>
    );
  }
}

const mapDispatchToProps = {
  loginSuccess,
};

export default connect(null, mapDispatchToProps)(Login);
