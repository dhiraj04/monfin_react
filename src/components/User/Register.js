import React, { Component } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import axiosInstance from "../../utils/axiosInstance";

export class Register extends Component {
  state = {
    email: "",
    username: "",
    first_name: "",
    last_name: "",
    password: "",
    confirm_password: "",

    is_form_loading: false,
    error_message: "",
  };

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = (e) => {
    this.setState({
      is_form_loading: true,
    });
    let {
      first_name,
      last_name,
      username,
      email,
      password,
      confirm_password,
    } = this.state;

    let user_data = {
      first_name: first_name,
      last_name: last_name,
      email: email,
      username: username,
      password: password,
      confirm_password: confirm_password,
    };

    axiosInstance()
      .post("/user/register/", user_data)
      .then((res) => {
        this.setState({
          is_form_loading: false,
        });
        this.props.onHide();
      })
      .catch((err) => {
        if (err.response) {
          this.setState({
            is_form_loading: false,
            error_message: err.response.data,
          });
        }
      });
  };

  render() {
    const registerFormValid =
      !this.state.first_name?.length ||
      !this.state.last_name?.length ||
      !this.state.email?.length ||
      !this.state.username?.length ||
      !this.state.password?.length ||
      !this.state.confirm_password?.length;

    return (
      <Form>
        <Modal show={true} onHide={this.props.onHide}>
          <Modal.Header closeButton>
            <Modal.Title>New User Registration</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group controlId="formFirstName">
              <Form.Label>First Name</Form.Label>
              <Form.Control
                value={this.state.first_name}
                onChange={this.onChange}
                name="first_name"
                type="text"
                placeholder="First Name"
                required
              />
            </Form.Group>

            <Form.Group controlId="formLastName">
              <Form.Label>Last Name</Form.Label>
              <Form.Control
                value={this.state.last_name}
                onChange={this.onChange}
                name="last_name"
                type="text"
                placeholder="Last Name"
                required
              />
            </Form.Group>

            <Form.Group controlId="formEmail">
              <Form.Label>Email</Form.Label>
              <Form.Control
                value={this.state.email}
                onChange={this.onChange}
                name="email"
                type="text"
                placeholder="Email"
                required
                isInvalid={!!this.state.error_message.email}
              />

              <Form.Control.Feedback type="invalid">
                {this.state.error_message.email }
              </Form.Control.Feedback>
            </Form.Group>

            <Form.Group controlId="formUsername">
              <Form.Label>Username</Form.Label>
              <Form.Control
                value={this.state.username}
                onChange={this.onChange}
                name="username"
                type="text"
                placeholder="Username"
                required
                isInvalid={!!this.state.error_message.username}
              />
              <Form.Control.Feedback type="invalid">
                {this.state.error_message.username }
              </Form.Control.Feedback>
            </Form.Group>

            <Form.Group controlId="formPassword">
              <Form.Label>
                Password{" "}
                <small className="text-muted">(Min. 8 Charaacters)</small>
              </Form.Label>
              <Form.Control
                value={this.state.password}
                onChange={this.onChange}
                name="password"
                type="password"
                placeholder="Password"
                required
                isInvalid={!!this.state.error_message.password}
              />
              <Form.Control.Feedback type="invalid">
                {this.state.error_message.password}
              </Form.Control.Feedback>
            </Form.Group>

            <Form.Group controlId="formConfirmPassword">
              <Form.Label>Confirm Password</Form.Label>
              <Form.Control
                value={this.state.confirm_password}
                onChange={this.onChange}
                name="confirm_password"
                type="password"
                placeholder="Confirm Password"
                required
                isInvalid={!!this.state.error_message.confirm_password}
              />

              <Form.Control.Feedback type="invalid">
                {this.state.error_message.confirm_password}
              </Form.Control.Feedback>
            </Form.Group>
          </Modal.Body>

          <Modal.Footer>
            <Button variant="secondary" onClick={this.props.onHide}>
              Close
            </Button>
            <Button
              variant="primary"
              type="submit"
              disabled={registerFormValid || this.state.is_form_loading}
              onClick={this.onSubmit}
            >
              {this.state.is_form_loading ? "Registering New User" : "Register"}
            </Button>
          </Modal.Footer>
        </Modal>
      </Form>
    );
  }
}

export default Register;
