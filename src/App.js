import "./App.css";
import { Component } from "react";
import { Router, Route, Switch } from "react-router-dom";
import Login from "./components/User/Login";
import "bootstrap/dist/css/bootstrap.min.css";
import "react-toastify/dist/ReactToastify.css";
import "react-datepicker/dist/react-datepicker.css";
import Dashboard from "./components/Dashboard/Dashboard";
import ProtectedRoute from "./utils/ProtectedRoute";
import { history } from "./history";
import Money from "./components/Dashboard/Money/Money";
import PayReceive from "./components/Dashboard/PayReceive/PayReceive";
import Dogs from "./components/Dogs";
import Profile from "./components/Dashboard/Profile/Profile";

class App extends Component {
  render() {
    return (
      <Router history={history}>
        <Switch>
          <ProtectedRoute exact path="/dashboard" component={Dashboard} />
          <ProtectedRoute exact path="/money" component={Money} />
          <ProtectedRoute exact path="/pay-receive/" component={PayReceive}/>
          <ProtectedRoute exact path="/profile/" component={Profile}/>

          <Route exact path="/dogs/" component={Dogs} />
          <Route exact path="/login" component={Login} />

          <Route component={Login} />
        </Switch>
      </Router>
    );
  }
}

export default App;
