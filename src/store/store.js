import { configureStore } from "@reduxjs/toolkit";
import accountGroupReducer from "../features/userSlice/accountGroupSlice";
import accountReducer from "../features/userSlice/accoutSlice";
import incomeReducer from "../features/userSlice/IncomeSlice";
import profileReducer from "../features/userSlice/ProfileSlice";
import totalAmountReducer from "../features/userSlice/TotalAmountSlice";
import usersReducer from "../features/userSlice/userSlice";

// const combinedRecucer = combineReducers({
//   users: usersReducer,
//   profile: profileReducer,
//   accounts: accountReducer,
//   accountGroup: accountGroupReducer,
//   income: incomeReducer,
//   totalAmount: totalAmountReducer,
// });

// export default configureStore({
//     reducer: combinedRecucer
//   });

export default configureStore({
  reducer: {
    users: usersReducer,
    profile: profileReducer,
    accounts: accountReducer,
    accountGroup: accountGroupReducer,
    income: incomeReducer,
    totalAmount: totalAmountReducer,
  },
});
