import { createSlice } from "@reduxjs/toolkit"


const accountSlice = createSlice({
    name: "accounts",
    initialState: {
        data: null,
    },
    reducers: {
        accountData: (state, action) => {
            state.data = action.payload
        },
        accountDataSuccess: (state, action) => {
            state.data.push(action.payload)
        },
        accountEditDataSuccess: (state, action) => {
            const index = state.data.findIndex(data => data.id === action.payload.id)
            state.data[index].account_name = action.payload.account_name
            state.data[index].account_group = action.payload.account_group
          },
    }
});

export const { accountData, accountDataSuccess, accountEditDataSuccess } = accountSlice.actions;

const accountReducer = accountSlice.reducer;
export default accountReducer;