import { createSlice } from "@reduxjs/toolkit";

const accountGroupSlice = createSlice({
  name: "accountGroup",
  initialState: {
    data: null,
  },
  reducers: {
    accountGroupData: (state, action) => {
      state.data = action.payload;
    },
    accountGroupDataSuccess: (state, action) => {
      state.data.push(action.payload);
    },
    accountGroupEditDataSuccess: (state, action) => {
      const index = state.data.findIndex(data => data.id === action.payload.id)
      state.data[index] = action.payload
    },
  },
});

export const {
  accountGroupData,
  accountGroupDataSuccess,
  accountGroupEditDataSuccess,
} = accountGroupSlice.actions;

const accountGroupReducer = accountGroupSlice.reducer;
export default accountGroupReducer;
