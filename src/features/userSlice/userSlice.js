import { createSlice } from "@reduxjs/toolkit";
import jwtDecode from "jwt-decode";

const userSlice = createSlice({
  name: "users",
  initialState: {
    token: null,
    data: null,
  },
  reducers: {
    loginSuccess: (state, action) => {
      state.token = action.payload;
      state.data = jwtDecode(action.payload.token)
    },
    logout: (state, action) => {
      state.data = null
    }
  },
});

export const { loginSuccess, logout } = userSlice.actions;

const usersReducer = userSlice.reducer;
export default usersReducer;
