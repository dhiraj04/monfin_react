import { createSlice } from "@reduxjs/toolkit";
import { logout } from "./userSlice";


const profileSlice = createSlice({
    name: "profile",
    initialState: {
      data: null,
    },
    reducers: {
      profileData: (state, action) => {
        state.data = action.payload;
      }
    },
    extraReducers: (builder) => {
      builder.addCase(logout, (state, action) => {
        state.data = null;
      });
    }
  });
  
  export const { profileData } = profileSlice.actions;

  const profileReducer = profileSlice.reducer;
export default profileReducer;
