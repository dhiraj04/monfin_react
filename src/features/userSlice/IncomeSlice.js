import { createSlice } from "@reduxjs/toolkit"


const incomeSlice = createSlice({
    name: "income",
    initialState: {
        data: null,
    },
    reducers: {
        incomeData: (state, action) => {
            state.data = action.payload
        },
        incomeDataSuccess: (state, action) => {
            state.data.push(action.payload)
        },
        incomeEditDataSuccess: (state, action) => {
            const index = state.data.findIndex(data => data.id === action.payload.id)
            state.data[index].account_name = action.payload.account_name
            state.data[index].account_group = action.payload.account_group
          },
    }
});

export const { incomeData, incomeDataSuccess, incomeEditDataSuccess } = incomeSlice.actions;

const incomeReducer = incomeSlice.reducer;
export default incomeReducer;