import { createSlice } from "@reduxjs/toolkit";
import { logout } from "./userSlice";

const totalAmountSlice = createSlice({
  name: "totalAmount",
  initialState: {
    data: null,
  },
  reducers: {
    totalAmountData: (state, action) => {
      state.data = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(logout, (state, action) => {
      state.data = null;
    });
  },
});

export const { totalAmountData } = totalAmountSlice.actions;

const totalAmountReducer = totalAmountSlice.reducer;
export default totalAmountReducer;
